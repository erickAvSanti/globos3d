<div class="modals">
	<div id="modal_bienvenida" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<?php 
	  		if ( $detect->isMobile() ) {
 				$_modal = "sm";
			}else{
 				$_modal = "sm";
 			}
	  	?>
	  	<div class="modal-dialog modal-<?php echo $_modal;?>"> 
	    	<!-- Modal content-->
	    	<div class="modal-content">
	      		<div class="modal-header"> 
	        		<h4 class="modal-title cFont cColor1">Globos 3D | Revienta la mayor cantidad de globos posibles.</h4>
	      		</div>
	      		<div class="modal-body">
	      			<ul class="nav nav-tabs">
				  		<li class="active cFont"><a data-toggle="tab" href="#home">Inicio</a></li>
				  		<li class="cFont"><a data-toggle="tab" href="#menu1">Uso/Config</a></li>
				  		<li class="cFont"><a data-toggle="tab" href="#menu2">Extras</a></li>
					</ul>

					<div class="tab-content">
				  		<div id="home" class="tab-pane fade in active"> 
				  			<br />
			      			<p class="cFont">
			      				Te invito a reventar la mayor cantidad de globos en este "revienta globos virtual".<br />
			      				Perfecto ! Click en "Jugar Ahora", pero antes debes elegir el modo entre: "Fácil","Intermedio","Complicado".
			      			</p> 
			      			<p class="cFont">
			      				Aún tienes dudas de cómo se juega ?. Visualiza el siguiente video: <a href="https://youtu.be/f2WXNwFe3Bc" target="_blank">aquí</a>
			      			</p>
			      			<div id="cplay">
			      				<div class="btn-group" role="group" aria-label="...">
							  		<button type="button" class="btn cFont s_level selected" level="1">Fácil</button>
							  		<button type="button" class="btn cFont s_level" level="2">Intermedio</button>
							  		<button type="button" class="btn cFont s_level" level="3">Complicado</button>
								</div><br /><br />

			      				<div class="btn btn-default cFont" id="play">Jugar ahora</div>
			      			</div>
			      			<div class="fb-like" data-href="https://facebook.com/arquigames/" data-layout="standard" data-action="like" data-size="small" data-show-faces="true"></div>
			      			<div class="ad-embebed-msg">Anuncio</div>
			        		<div id="ct-1"></div>  
				  		</div>
				  		<div id="menu1" class="tab-pane fade">
				  			<br />
				    		<div class="media">
						  		<div class="media-left">
						    		<a href="#">
						      			<img class="media-object" src="./imgs/boton_maximize.png" alt="Botón maximizar">
						   		 	</a>
						  		</div>
						  		<div class="media-body cFont">
						  			Habilita/Inhabilita el modo pantalla completa.
						  		</div>
							</div> 
							<div>
								<ul class="cFont"> 
									<li>Teclas A,D: Desplazamiento lateral.</li>
									<li>Teclas W,S: Desplazamiento adelante/atrás.</li> 
									<li>Mouse: Click sobre la pantalla para entrar en modo bloqueo de pantalla y 
										controlarás la cámara moviendo el mouse. Si estás en un dispositivo 
										con soporte táctil, esto quizas no funcione y 
										tendrás que deslizar tus dedos o el mouse por la pantalla para
										 simular el movimiento de la cámara.
									</li>
									<li>Click en pantalla para reventar un globo al que se le apunta.</li>
									<li>
										<span>Niveles</span>
										<ul>
											<li>Fácil: Si 1 globo colisiona con el piso, entonces el juego finaliza.</li>
											<li>Intermedio: Si al menos 5 globos colisiona con el piso, entonces el juego finaliza.</li>
											<li>Complicado: Si al menos 10 globos colisiona con el piso, entonces el juego finaliza.</li>
										</ul>
									</li>
								</ul>
							</div>
							<div>
								<label class="cFont">Volumen:</label><br />
								<div id="master_volume_icon" class="fa fa-volume-up" aria-hidden="true"></div>
								<div id="master_volume"></div> 
							</div><br />
							<div>
								<label class="cFont">Velocidad del movimiento táctil:</label><br />
								<div id="master_hand_icon" class="fa fa-hand-pointer-o" aria-hidden="true"></div>
								<div id="master_hand"></div> 
							</div>
				  		</div>
				  		<div id="menu2" class="tab-pane fade">
				  			<br />
				    		<ul> 
				    			<li>
				    				Información WebGL/GPU/CPU:
				    				<ul>
				    					<li id="cpu_info1"></li>
				    					<li id="cpu_info2"></li>
				    					<li id="cpu_info3"></li>
				    					<li id="cpu_info4"></li>
				    					<li id="cpu_info5"></li> 
				    				</ul>
				    			</li>
				    			<li>Desarrollado por: <a href="http://arquigames.pe" target="_blank">arquigames</a> Versión 1.4.21</li>
				    		</ul>
				  		</div>
					</div>	
	      		</div>
	      		<div class="modal-footer"> 
	      		</div>
	    	</div> 
	  	</div>
	</div>
	<div id="modal_lost" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-md"> 
	    	<!-- Modal content-->
	    	<div class="modal-content">
	      		<div class="modal-header"> 
	        		<h4 class="modal-title cFont cColor1">Globos 3D | Fin de juego</h4>
	      		</div>
	      		<div class="modal-body"> 
	      			<div class="cFont" style="text-align: center;">Nivel: <span id="l_lvl"></span>.</div>
	      			<div class="cFont cc_l">
	      				Lograste reventar <span id="l_points"></span> globos !
	      			</div><br />
	      			<div class="cFont" style="text-align:center;padding:5px;">
	      				<div class="btn btn-default" id="continue">Continuar</div>
	      			</div>
	      		</div>
	      		<div class="modal-footer"> 
	      		</div>
	    	</div> 
	  	</div>
	</div>
</div>
<div style="z-index:-10;position:absolute;">
	<label id="txt1" class="cFont"></label>
</div>
<script>
  	$( function() { 
	    $( "#master_volume" ).slider({
	      	value: 50,
	      	orientation: "horizontal",
	      	range: "min",
	      	animate: true,
	      	change: function( event, ui ) { 
	      		//console.log(ui.value);
	      		if(ui.value==0){
	      			$("#master_volume_icon").removeClass("fa-volume-up").addClass("fa-volume-off");
	      		}else if(ui.value==100){
	      			$("#master_volume_icon").removeClass("fa-volume-off").addClass("fa-volume-up");
	      		}else{}
	      	}
	    });  
	    $("#master_volume_icon").click(function(evt){ 
	    	var _this = $(this);
	    	if(_this.hasClass("fa-volume-up")){
	    		_this.removeClass("fa-volume-up").addClass("fa-volume-off");
	    		$( "#master_volume" ).slider({value: 0});
	    	}else{
	    		_this.removeClass("fa-volume-off").addClass("fa-volume-up");
	    		$( "#master_volume" ).slider({value: 100});
	    	} 
	    });
	    $( "#master_hand" ).slider({
	      	value: 0,
	      	orientation: "horizontal",
	      	range: "min",
	      	animate: true 
	    });  
   });
  	$(window).ready(
  		function(_evt){
  			$(".s_level").click(function(_evt){
	  			var _el = $(".s_level");
	  			_el.each(
	  				function(idx,obj){
	  					var _obj = $(obj);
	  					_obj.removeClass("selected");
	  				}
	  			);
	  			var _this = $(this);
	  			_this.addClass("selected");
	  			var _lvl = _this.attr("level");
	  			window._lib_lvl = parseInt(_lvl);
  			});
  			$("#continue").click(function(_evt){
  				$("#modal_lost").modal("hide");
  			});
  			$('#modal_lost').on('hidden.bs.modal', function () {
  				$("#modal_bienvenida").modal("show");
			})
  		}
  	);
  </script>