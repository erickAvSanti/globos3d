var appgame = appgame || {};

appgame.CameraBounding = (
	function(){

		var _instance = null;  

		var _boundingCylinder = {
			radius:0.3,
			minY:-1.5,
			maxY:1.5,
			center:null/*current camera position*/
		};

		var _halfPI = Math.PI/2;

		var _boundingSphere = {
			radius:Math.sqrt(Math.pow(_boundingCylinder.radius,2)+Math.pow(_boundingCylinder.maxY,2)),
			center:null/*current camera position*/
		}; 

		var _tmpv1 = new THREE.Vector3();
		var _tmpv2 = new THREE.Vector3();
		var _tmpv3 = new THREE.Vector3();

		var _up = new THREE.Vector3(0,1,0);

		var _cameraControl = undefined;

		CameraBounding = function(camCtrl){
			_cameraControl = camCtrl;
		};
		CameraBounding.prototype = {
			constructor:CameraBounding, 
			test:function(){
				console.log("hello from CameraBounding");
			},
			start:function(){ 
			}, 
			checkCollisions:function(){ 
			}, 
			composeVector3:function(v1,v2,scale){
				return new THREE.Vector3(
						v1.x*scale + v2.x * scale,
						v1.y*scale + v2.y * scale,
						v1.z*scale + v2.z * scale
					); 
			},
			obtenerVectorComponente:function(a,b){
				b = b.clone();
				b.normalize();
				var _scalar = a.x*b.x + a.y*b.y+a.z*b.z;
				return b.multiplyScalar(_scalar);
			},
			intersectionSphereSphere:function(s1_center,s1_radius,s2_center,s2_radius){
				return s1_center.distanceTo(s2_center)<=(s1_radius+s2_radius);
			}
		};

		return {
			instance:function(camCtrl){
				if(!_instance){
					_instance = new CameraBounding(camCtrl);
					return _instance;
				}
				return null;
			}
		};
	}
)();
