var appgame = appgame || {};

appgame.OrbitCamera = (
	function(){

		var _instance = null; 


		var _btn_src = "./imgs/boton_orbitar_camara.png"; 

		var _btn = null; 

		var _width = 90;
		var _height = 90;

		var _radius = 30;

		var _width2 = _radius*2;
		var _height2 = _radius*2;

		var _point = {x:10,y:10,offsetX:10,offsetY:100};

		var _play = false;
		var _touch_play = false;
		var _distance = 10;
		//var _angle = Math.PI/18;
		var _angle = Math.PI/180;
		var _up = new THREE.Vector3(0,1,0);
		var _axisX = new THREE.Vector3(1,0,0);
		var _axisZ = new THREE.Vector3(0,0,1);
		var _arrowHelper = new THREE.ArrowHelper( _up,new THREE.Vector3(),5,0x00ff00 );
		var _arrowHelper1 = new THREE.ArrowHelper( _axisX,new THREE.Vector3(),5,0xff0000 );
		var _arrowHelper2 = new THREE.ArrowHelper( _axisZ,new THREE.Vector3(),5,0x0000ff );
		_arrowHelper.visible =false;
		_arrowHelper1.visible =false;
		_arrowHelper2.visible =false;

		var _touch_identifier = null;

		var _supportsTouch = null;
		

		var _debug = false;
		OrbitCamera = function(){

		};
		OrbitCamera.prototype = {
			constructor:OrbitCamera, 
			test:function(){
				console.log("hello from OrbitCamera object");
			},
			start:function(){
				var _this = this; 
				$(window).keyup(
					function(_evt){
						_this.listenKeyUp(_evt);
					}
				); 
				if(w_wgl && w_wgl.scene){
					w_wgl.scene.add(_arrowHelper);
					w_wgl.scene.add(_arrowHelper1);
					w_wgl.scene.add(_arrowHelper2);
				}
				this.cargarSprites();
				this.configurarPosicionSprites();

			},
			supportsTouch:function(){
				if(_supportsTouch===null){
					_supportsTouch = "ontouchstart" in window;
				}
				return _supportsTouch;
			}, 
			listenKeyUp:function(_evt){
				var _keyCode = _evt.which || _evt.keyCode;  
				if(_keyCode==79){//keyO
					_play = !_play;
				}
			},
			render:function(){ 
				if(_play || _touch_play){
					this.animate(); 
				}else{
					if(_arrowHelper.visible)_arrowHelper.visible = false;
					if(_arrowHelper1.visible)_arrowHelper1.visible = false;
					if(_arrowHelper2.visible)_arrowHelper2.visible = false;
				}
				this.dibujarSprites();

			},
			animate:function(){
				if(w_wgl){
					var _wdir = w_wgl.camera.getWorldDirection().clone(); 
					var _v3a = this.obtenerVectorComponente(_wdir,_up);
					if(_debug)console.log("componente",_v3a);
					_wdir.sub(_v3a); 
					_wdir.normalize();//vector dirección unitario 
					var _v3b = new THREE.Vector3();//vector distante al punto de rotación
					_v3b.addScaledVector(_wdir,_distance);//vector dirección
					var _v3c = w_wgl.camera.position.clone();
					_v3c.add(_v3b);
					if(_arrowHelper){
						_arrowHelper.position.copy(_v3c);
						_arrowHelper.visible = true;
					}
					if(_arrowHelper1){
						_arrowHelper1.position.copy(_v3c);
						_arrowHelper1.visible = true;
					}
					if(_arrowHelper2){
						_arrowHelper2.position.copy(_v3c);
						_arrowHelper2.visible = true;
					}
					if(_debug)console.log("antes",_v3b);
					_v3b.applyAxisAngle(_up.clone(),_angle);
					if(_debug)console.log("después",_v3b);
					_v3c.sub(_v3b);
					if(_debug)console.log("posición",_v3c);
					_wdir = w_wgl.camera.getWorldDirection().clone(); 
					w_wgl.camera.position.copy(_v3c);
					_wdir.applyAxisAngle(_up.clone(),_angle).normalize().add(_v3c);
					w_wgl.camera.lookAt(_wdir);
				}
			},
			obtenerVectorComponente:function(a,b){
				b = b.clone();
				b.normalize();
				var _scalar = a.x*b.x + a.y*b.y+a.z*b.z;
				return b.multiplyScalar(_scalar);
			},
			resize:function(){
				this.configurarPosicionSprites();
			},
			cargarSprites:function(){
				if(!w_ctouch_ctx)return; 
				if(!this.supportsTouch())return; 
				_btn = new Image;
				_btn.onload=function(){
					_btn._loaded = true;
				};	
				_btn.src = _btn_src;
			},
			dibujarSprites:function(){ 
				if(w_ctouch_ctx && _btn && _btn._loaded){  
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.drawImage(_btn,0,0,_width,_height,_point.x,_point.y,_width2,_height2);
					w_ctouch_ctx.closePath();
				}
			},
			configurarPosicionSprites:function(){
				_point.x = w_width - _width2 - _point.offsetX;
				_point.y = _point.offsetY;
			},
			touch:function(_what,_evt){
				if(_what=="s")this.touch_start(_evt);
				if(_what=="e")this.touch_end(_evt);
				if(_what=="c")this.touch_cancel(_evt);
				if(_what=="l")this.touch_leave(_evt);
				if(_what=="m")this.touch_move(_evt);
			},
			touch_point_collide:function(_touch){
				if(_touch.clientX && _touch.clientY){
					var _v2a = new THREE.Vector2(_point.x+_width2/2,_point.y+_height2/2);
					var _v2b = new THREE.Vector2(_touch.clientX,_touch.clientY);
					if(_v2a.distanceTo(_v2b)<=_radius){ 
						return true;
					}  
				}
				return false;
			},
			touch_start:function(_evt){ 
				if(_touch_identifier===null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(this.touch_point_collide(_touch)){
							_touch_play = true;
							_touch_identifier = _touch.identifier;
							w_ctouch._lib_orbitcamera_clicked = true;
						}
					} 
				} 
			},
			touch_move:function(_evt){  
				if(_touch_identifier!==null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(
							_touch_identifier==_touch.identifier 
						){
							if(this.touch_point_collide(_touch)){
								_touch_play = true; 
							}else{
								_touch_play = false;
							}
						} 
					} 
				} 
			},
			touch_end:function(_evt){  
				if(_touch_identifier!==null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx]; 
						if(_touch_identifier==_touch.identifier){
							_touch_play = false;
							_touch_identifier = null;
							w_ctouch._lib_orbitcamera_clicked = false;
						}
					} 
				}  
			},
			touch_cancel:function(_evt){  
				this.touch_end(_evt);
			},
			touch_leave:function(_evt){  
				this.touch_end(_evt);
			}
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new OrbitCamera();
					return _instance;
				}
				return null;
			}
		};
	}
)();
