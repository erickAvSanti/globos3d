var appgame = appgame || {};

appgame.ZonaA = (
	function(){

		var _instance = null;
		var _loader = new THREE.JSONLoader();

		var _elements = {}; 
		_elements["piso"] = "./modelos/piso.json"; 
		_elements["nube_front"] = "./modelos/nube_front.json"; 
		_elements["nube_tras"] = "./modelos/nube_tras.json"; 
		_elements["nube_izq"] = "./modelos/nube_izq.json"; 
		_elements["nube_der"] = "./modelos/nube_der.json"; 
		_elements["bloque_front"] = "./modelos/bloque_front.json"; 
		_elements["malla_der"] = "./modelos/malla_der.json"; 
		_elements["malla_izq"] = "./modelos/malla_izq.json"; 
		_elements["globo"] = "./modelos/globo.json"; 

		window._lib_globos = [];
		var _globo_materials = [];
		var _mat_index = 0;

		var _globo_master = null;

		var _interval_add_id = null;

		window._sounds_ogg = [];
		window._sounds_m4a = [];  

		var _audioLoader = new THREE.AudioLoader();

		ZonaA = function(){

		};
		ZonaA.prototype = {
			constructor:ZonaA,
			cargarElementos:function(){ 
				// load a resource
				for(var _idx in _elements){
					this.loadFile(_idx,_elements[_idx]);
				}
				var _this = this;
				//_this.setInterval(); 
				$(window).blur(
					function(evt){ 
						_this.blur();
					}
				);
				$(window).focus(
					function(evt){
						_this.focus();
					}
				); 
				$(w_ctouch).click(
					function(_evt){
						_this.setInterval(); 
					}
				);

				this.loadSound(); 

			},
			loadSound:function(){
				var audio_listener = new THREE.AudioListener();
				w_wgl.camera.add( audio_listener );

				_audioLoader.load( 'audio/ballo_pop.ogg', function( buffer ) {
					
					for (var i = 0; i < 4; i++) {
						var _sound = new THREE.Audio( audio_listener ); 
						_sound.setBuffer( buffer );
						_sound.setLoop( false );
						_sound.setVolume( 1 ); 
						_sounds_ogg.push(_sound);
					}  
				});
			},
			blur:function(){
				if(_interval_add_id){
					window.clearInterval(_interval_add_id);
					_interval_add_id = null;
				}
			},
			focus:function(){
				this.setInterval();
			},
			setInterval:function(){
				if(!_interval_add_id)_interval_add_id = window.setInterval(_instance.addGlobo,1500);
			},
			loadFile:function(idx,obj){
				_loader.load( 
					obj, 
					// Function when resource is loaded
					function ( geometry, materials ) { 
						var object;  
						for(var _index in materials){
							var _mat = materials[_index];
							if(idx=="globo"){ 
								_mat.morphTargets = true; 
							}
							_mat.side = THREE.FrontSide;
						} 
						
						if(idx=="globo"){ 
							_globo_materials = materials;
							object = new THREE.Mesh( geometry, materials[0] ); 
							console.log("globo started");
							object.position.y = 15; 
							window._globo_mast = _globo_master = object;
							object._lib_mixer = new THREE.AnimationMixer( object );
							var clip = THREE.AnimationClip.CreateFromMorphTargetSequence( 'globo', geometry.morphTargets, 30 );
							object._lib_mixer.clipAction( clip ).setDuration( 1 ).play();
						}else{
							object = new THREE.Mesh( geometry, materials ); 
							if(idx=="piso"){
								object.receiveShadow = true;
							}else{
								//object.castShadow = true;
							}
							w_wgl.scene.add( object ); 
						}  
					},
					function(){

					},
					function(xhr){
						console.log(xhr);
					}
				);
			},
			cancelLoad:function(){

			},
			addGlobo:function(){
				var _pass = w_wgl && w_wgl.scene && _globo_master;
				if(!_pass)return; 
				var _globo,_find=false;
				for(var _idx in _lib_globos){
					_globo = _lib_globos[_idx];
					if(!_globo.visible){
						_find = true;
						break;
					}
				}
				if(!_find){
					_globo = _globo_master.clone(); 
					_globo.castShadow = true;
					//----------------------------------------------------------------
					_globo._lib_mixer = new THREE.AnimationMixer( _globo );
					var clip = THREE.AnimationClip.CreateFromMorphTargetSequence( 'globo', _globo.geometry.morphTargets, 30 );
					_globo._lib_mixer.clipAction( clip ).setDuration( 1 ).play();
					_globo._lib_boundingSphere1 = new THREE.Sphere(new THREE.Vector3(),3.1);
					_globo._lib_boundingSphere2 = new THREE.Sphere(new THREE.Vector3(0,1.88041,0),2.69594);
					_globo._lib_boundingSphere3 = new THREE.Sphere(new THREE.Vector3(0,-1.88041,0),2.69594);
					//----------------------------------------------------------------
					_lib_globos.push(_globo);
					w_wgl.scene.add(_globo);
				}
				var _randX = (Math.random()-0.5)*50;
				var _randZ = (Math.random()-0.5)*50;
				_randX = THREE.Math.clamp(_randX,-30,30);
				_randZ = THREE.Math.clamp(_randZ,-30,30);
				_globo.position.set(_randX,50,_randZ);
				_globo.visible =true; 
				_globo.material = _globo_materials[_mat_index];
				_mat_index++;
				if(_mat_index>_globo_materials.length-1){
					_mat_index = 0;
				}

			},
			render:function(){
				for(var _idx in _lib_globos){
					var _globo = _lib_globos[_idx];
					if(_globo.visible){
						if(_globo && _globo._lib_mixer){
							_globo._lib_mixer.update( 0.01 );
						}
						_globo.position.y-=0.3;
						if(_globo.position.y<=1){
							_globo.visible = false;
						}
					} 
				}
				this.checkCollisions();
			},
			checkCollisions:function(){ 
				if(!_lib_shooter_balls)return;
				for(var _idx in _lib_shooter_balls){
					var _ball = _lib_shooter_balls[_idx];
					if(_ball.visible){ 
						this.checkCollisionsWithGlobos(_ball);
					} 
					if(!_ball.visible)continue;
				}
			},
			checkCollisionsWithGlobos:function(_ball){  
				if(!_lib_globos)return;  
				if(!_ball.geometry.boundingSphere){
					_ball.geometry.computeBoundingSphere();
				}  
				for(var _idx in _lib_globos){
					var _globo = _lib_globos[_idx];
					if(_globo.visible){ 
						var _bB1 = _globo._lib_boundingSphere1.clone();
						var _bB2 = _globo._lib_boundingSphere2.clone();
						var _bB3 = _globo._lib_boundingSphere3.clone();

						_bB1.center.add(_globo.position);
						_bB2.center.add(_globo.position);
						_bB3.center.add(_globo.position); 

						if(
							(_bB1.distanceToPoint(_ball.position)< (_ball.geometry.boundingSphere.radius + _bB1.radius)) || 
							(_bB2.distanceToPoint(_ball.position)< (_ball.geometry.boundingSphere.radius + _bB2.radius)) || 
							(_bB3.distanceToPoint(_ball.position)< (_ball.geometry.boundingSphere.radius + _bB3.radius))
						){	
							_globo.visible = false;
							if(_sounds_ogg.length>0){ 
								for (var i = 0; i < _sounds_ogg.length; i++) {
									var _sound = _sounds_ogg[i];
									if(!_sound.isPlaying){
										_sound.play();
										break;
									}
								} 
							}
							break;
						}

					} 
				}
			},
			test:function(){
				console.log("hello from ZonaA object");
			}
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new ZonaA();
					return _instance;
				}
				return null;
			}
		};
	}
)();
