var appgame = appgame || {};

appgame.ZonaA = (
	function(){

		var _instance = null;
		var _loader = new THREE.JSONLoader();

		var _elements = {}; 
		_elements["piso"] = "./modelos/piso.json"; 
		_elements["nube_front"] = "./modelos/nube_front.json"; 
		_elements["nube_tras"] = "./modelos/nube_tras.json"; 
		_elements["nube_izq"] = "./modelos/nube_izq.json"; 
		_elements["nube_der"] = "./modelos/nube_der.json"; 
		_elements["bloque_front"] = "./modelos/bloque_front.json"; 
		_elements["bloque_tras"] = "./modelos/bloque_tras.json"; 
		_elements["malla_der"] = "./modelos/malla_der.json"; 
		_elements["malla_izq"] = "./modelos/malla_izq.json"; 
		_elements["globo"] = "./modelos/globo.json"; 

		window._lib_globos = [];
		var _globo_materials = [];
		var _mat_index = 0;

		var _globo_master = null;

		var _interval_add_id = null;

		window._sounds_ogg = [];
		window._sounds_m4a = [];  

		var _audioLoader = new THREE.AudioLoader();

		var _elapsed_time_globo_def = 1500;
		var _elapsed_time_globo = _elapsed_time_globo_def;
		var _elapsed_time_globo_min_facil = 900;
		var _elapsed_time_globo_min_medio = 750;
		var _elapsed_time_globo_min_dificil = 500;
		var _elapsed_time_globo_dec_facil = 10;
		var _elapsed_time_globo_dec_medio = 50;
		var _elapsed_time_globo_dec_dificil = 200;

		var _contador_globos_reventados = 0;
		var _contador_globos_no_reventados = 0;

		var _text_opacity_def = 0.6;
		var _text_opacity = 0;
		var _text_opacity_dec = 0.06;
		var _text_font_size_def = 180;
		var _text_font_size = _text_font_size_def;
		var _text_font_size2 = 40;
		var _text_font_size_dec = 5;

		var _current_lvl = 1;

		ZonaA = function(){

		};
		ZonaA.prototype = {
			constructor:ZonaA,
			cargarElementos:function(){ 
				// load a resource
				for(var _idx in _elements){
					this.loadFile(_idx,_elements[_idx]);
				}
				var _this = this;
				//_this.setInterval(); 
				$(window).blur(
					function(evt){ 
						_this.blur();
					}
				);
				$(window).focus(
					function(evt){
						_this.focus();
					}
				); 
				$(w_ctouch).click(
					function(_evt){
						_evt.preventDefault();
						_this.checkCollisions();
					}
				);

				this.loadSound(); 

			},
			loadSound:function(){
				var audio_listener = new THREE.AudioListener();
				w_wgl.camera.add( audio_listener );

				_audioLoader.load( 'audio/ballo_pop2.ogg', function( buffer ) {
					
					for (var i = 0; i < 4; i++) {
						var _sound = new THREE.Audio( audio_listener ); 
						_sound.setBuffer( buffer );
						_sound.setLoop( false );
						_sound.setVolume( 0.5 ); 
						_sounds_ogg.push(_sound);
					}  
					$( "#master_volume" ).on( "slidechange",function(event, ui){
						for (var i = 0; i < 4; i++) {
							var _sound = _sounds_ogg[i];
							_sound.setVolume( ui.value/100 ); 
						}  
					});
				});
			},
			blur:function(){
				if(_interval_add_id){ 
					window.clearInterval(_interval_add_id);
					_interval_add_id = null;
				}
			},
			focus:function(){
				this.setInterval();
			},
			setInterval:function(){
				if(_lib_lvl && _lib_lvl!=1 && _lib_lvl!=2 && _lib_lvl!=3)return;
				if(_lib_game_paused)return;
				if(_lib_game_finish)return;
				if(!_interval_add_id){ 
					_interval_add_id = window.setInterval(_instance.addGlobo,_elapsed_time_globo);
				}
			},
			loadFile:function(idx,obj){
				_loader.load( 
					obj, 
					// Function when resource is loaded
					function ( geometry, materials ) { 
						var object;  
						for(var _index in materials){
							var _mat = materials[_index];
							if(idx=="globo"){ 
								_mat.morphTargets = true; 
							}
							_mat.side = THREE.FrontSide;
						} 
						
						if(idx=="globo"){ 
							_globo_materials = materials;
							object = new THREE.Mesh( geometry, materials[0] ); 
							console.log("globo started");
							object.position.y = 15; 
							window._globo_mast = _globo_master = object;
							object._lib_mixer = new THREE.AnimationMixer( object );
							var clip = THREE.AnimationClip.CreateFromMorphTargetSequence( 'globo', geometry.morphTargets, 30 );
							object._lib_mixer.clipAction( clip ).setDuration( 1 ).play();
						}else{
							object = new THREE.Mesh( geometry, materials ); 
							if(idx=="piso"){
								if(isMobile && isMobile.apple.device)object.receiveShadow = true;
							}else{ 
								//object.castShadow = true;
							}
							w_wgl.scene.add( object ); 
						}  
					},
					function(){

					},
					function(xhr){
						console.log(xhr);
					}
				);
			},
			cancelLoad:function(){

			},
			addGlobo:function(){
				if(_lib_lvl && _lib_lvl!=1 && _lib_lvl!=2 && _lib_lvl!=3)return;
				if(_lib_game_paused)return;
				if(_lib_game_finish)return;
				var _pass = w_wgl && w_wgl.scene && _globo_master;
				if(!_pass)return; 
				var _globo,_find=false;
				for(var _idx in _lib_globos){
					_globo = _lib_globos[_idx];
					if(!_globo.visible){
						_find = true;
						break;
					}
				}
				if(!_find){
					_globo = _globo_master.clone(); 
					if(isMobile && isMobile.apple.device)_globo.castShadow = true;
					//----------------------------------------------------------------
					_globo._lib_mixer = new THREE.AnimationMixer( _globo );
					var clip = THREE.AnimationClip.CreateFromMorphTargetSequence( 'globo', _globo.geometry.morphTargets, 30 );
					_globo._lib_mixer.clipAction( clip ).setDuration( 1 ).play();
					_globo._lib_boundingSphere1 = new THREE.Sphere(new THREE.Vector3(),3.1);
					_globo._lib_boundingSphere2 = new THREE.Sphere(new THREE.Vector3(0,1.88041,0),2.69594);
					_globo._lib_boundingSphere3 = new THREE.Sphere(new THREE.Vector3(0,-1.88041,0),2.69594);
					//----------------------------------------------------------------
					_lib_globos.push(_globo);
					w_wgl.scene.add(_globo);
				}
				var _randX = (Math.random()-0.5)*50;
				var _randZ = (Math.random()-0.5)*50;
				_randX = THREE.Math.clamp(_randX,-30,30);
				_randZ = THREE.Math.clamp(_randZ,-30,30);
				_globo.position.set(_randX,70,_randZ);
				_globo.visible =true; 
				_globo.material = _globo_materials[_mat_index];
				_mat_index++;
				if(_mat_index>_globo_materials.length-1){
					_mat_index = 0;
				}

				if(_current_lvl==2){
					if(_elapsed_time_globo>_elapsed_time_globo_min_medio){
						_elapsed_time_globo-=_elapsed_time_globo_dec_medio; 
					}
				}else if(_current_lvl==3){
					if(_elapsed_time_globo>_elapsed_time_globo_min_dificil){
						_elapsed_time_globo -= _elapsed_time_globo_dec_dificil; 
					}
				}else{
					if(_elapsed_time_globo>_elapsed_time_globo_min_facil){
						_elapsed_time_globo-=_elapsed_time_globo_dec_facil; 
					}
				} 
				_instance.blur();
				_instance.focus();
			},
			checkLvl:function(){
				if(_lib_lvl!=_current_lvl){
					_current_lvl = _lib_lvl;
					this.reset();
				}
			},
			reset:function(){
				_elapsed_time_globo = _elapsed_time_globo_def;
				for(var _idx in _lib_globos){
					var _globo = _lib_globos[_idx];
					_globo.visible = false;
				} 
				_contador_globos_reventados = 0;
				_contador_globos_no_reventados = 0;
				$("#trev_v").html("0");
			},
			render:function(){
				if(_lib_lvl && _lib_lvl!=1 && _lib_lvl!=2 && _lib_lvl!=3)return;
				this.checkLvl();
				if(_lib_game_paused)return;
				if(_lib_game_finish)return;
				if(window._lib_game_reset){
					this.reset();
					window._lib_game_reset = false;
				}
				for(var _idx in _lib_globos){
					var _globo = _lib_globos[_idx];
					if(_globo.visible){
						if(_globo && _globo._lib_mixer){
							_globo._lib_mixer.update( 0.01 );
						}
						if(_current_lvl==2){
							_globo.position.y-=0.38;
						}else if(_current_lvl==3){
							_globo.position.y-=0.46;
						}else{
							_globo.position.y-=0.3;
						}
						if(_globo.position.y<=5){
							_globo.visible = false;
							_contador_globos_no_reventados--;
						}
					} 
				} 
				if(_lib_touchedForFire){
					this.checkCollisions();
				}
				this.drawTexts();
				if(_current_lvl==2){
					if(_contador_globos_no_reventados<=-5){
						window._lib_game_finish = true;
						window._lib_force_exit_pointer_lock = true;
						$("#l_points").html(_contador_globos_reventados);
						$("#l_lvl").html("Intermedio");
						$("#modal_lost").modal("show");
					}
				}else if(_current_lvl==3){
					if(_contador_globos_no_reventados<=-10){
						window._lib_game_finish = true;
						window._lib_force_exit_pointer_lock = true;
						$("#l_points").html(_contador_globos_reventados);
						$("#l_lvl").html("Complicado");
						$("#modal_lost").modal("show");
					}
				}else{
					if(_contador_globos_no_reventados<0){
						window._lib_game_finish = true;
						window._lib_force_exit_pointer_lock = true;
						$("#l_points").html(_contador_globos_reventados);
						$("#l_lvl").html("Fácil");
						$("#modal_lost").modal("show");
					}
				} 

			},
			drawTexts:function(){
				this.drawTextGlobosReventados();
				this.drawTextGlobosNoReventados();
			},
			drawTextGlobosReventados:function(){ 
				if(_text_opacity>0){
					var _el = $("#txt1");
					_el.css("font-size",_text_font_size+"px");
					_el.html(_contador_globos_reventados);
					w_ctouch_ctx.fillStyle = 'rgba(0,0,0,'+_text_opacity+')';  
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.font = _text_font_size+"px 'Fredoka One', cursive";
					w_ctouch_ctx.fillText(_contador_globos_reventados,w_width/2-_el.outerWidth()/2,w_height/2+_el.outerHeight()/4);
					w_ctouch_ctx.closePath();  
					_text_opacity-=_text_opacity_dec;
					_text_font_size-=_text_font_size_dec;
				}
			},
			drawTextGlobosNoReventados:function(){  
					var _el = $("#txt1");
					_el.css("font-size",_text_font_size2+"px");
					_el.html(_contador_globos_no_reventados);
					w_ctouch_ctx.fillStyle = 'rgba(255,0,0,0.8)';  
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.font = _text_font_size2+"px 'Fredoka One', cursive";
					w_ctouch_ctx.fillText(_contador_globos_no_reventados,w_width-_el.outerWidth() - 25,w_height - _el.outerHeight() - 20);
					w_ctouch_ctx.closePath();   
			},
			checkCollisions:function(){ 
				if(_lib_game_paused)return;
				if(_lib_game_finish)return;
				this.setInterval(); 
				this.rayCastGlobos(); 
			},
			rayCastGlobos:function(){  
				if(!w_wgl || !w_wgl.camera)return; 
				var _line = {
					start:w_wgl.camera.position.clone(),
					_lib_vunit:w_wgl.camera.getWorldDirection().clone().normalize()
				}; 
				if(!_lib_globos)return;   
				for(var _idx in _lib_globos){
					var _globo = _lib_globos[_idx];
					if(_globo.visible){   

						var _bB1 = _globo._lib_boundingSphere1.clone();
						var _bB2 = _globo._lib_boundingSphere2.clone();
						var _bB3 = _globo._lib_boundingSphere3.clone();

						_bB1.center.add(_globo.position);
						_bB2.center.add(_globo.position);
						_bB3.center.add(_globo.position); 

						if(
							this.line3SphereIntersecction(_line,_bB1) || 
							this.line3SphereIntersecction(_line,_bB2) || 
							this.line3SphereIntersecction(_line,_bB3)
						){
							_globo.visible = false;
							_contador_globos_reventados ++;
							$("#trev_v").html(_contador_globos_reventados);
							if(_sounds_ogg.length>0){ 
								for (var i = 0; i < _sounds_ogg.length; i++) {
									var _sound = _sounds_ogg[i];
									if(!_sound.isPlaying){
										_sound.play();
										break;
									}
								} 
							}
							_text_opacity = _text_opacity_def;
							_text_font_size = _text_font_size_def;
							break;
						} 
					} 
				}
			},
			line3SphereIntersecction:function(line3,_sphere){
				var _r2 = 0;
				var _o = line3.start;
				var _c = 0;
				var _l = line3._lib_vunit;
				if(_sphere instanceof THREE.Sphere){
					_r2 = Math.pow(_sphere.radius,2);
					_c = _sphere.center.clone();
				}else{
					return false;
				}

				var _oc1 = _c.clone().sub(_o);
				_oc1 = Math.pow(_oc1.x,2)+Math.pow(_oc1.y,2)+Math.pow(_oc1.z,2);

				var _oc2 = _c.clone().sub(_o);
				_oc2 = _oc2.dot(_l);
				_oc2 = Math.pow(_oc2,2);

				var _result = _oc2 - _oc1 + _r2;
				if(_result>=0){return true;}
				return false;
			},
			test:function(){
				console.log("hello from ZonaA object");
			}
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new ZonaA();
					return _instance;
				}
				return null;
			}
		};
	}
)();
