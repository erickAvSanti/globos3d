var appgame = appgame || {};

appgame.Shooter = (
	function(){

		var _instance = null;  

		window._lib_shooter_balls = [];
		var _ball_velocity = 50/1000;

		var _ball_instance = undefined;

		var _ball_distance_camera = 2;

		var _dt = 20/1000;

		var _parent = undefined;//CameraControl

		var _aproxZero = 0.00001;
 

		var _interval_mousedown_id = -1;

		var _mousedown = function(){
			if(_interval_mousedown_id==-1){
				_interval_mousedown_id = setInterval(_instance.listen, 100);
			}
     		
		};
		var _mouseup = function(){
			if(_interval_mousedown_id!=-1){
		     	clearInterval(_interval_mousedown_id);
		     	_interval_mousedown_id=-1;
		   	}
		};

		Shooter = function(_p){
			_parent = _p;
		};
		Shooter.prototype = {
			constructor:Shooter, 
			test:function(){
				console.log("hello from Shooter");
			},
			start:function(){  
				if(w_wgl.scene){
					var geometry = new THREE.SphereGeometry( 0.2, 8, 8 );
					var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
					material.opacity = 0.2;
					material.transparent = true;
					var sphere = new THREE.Mesh( geometry, material ); 
					_ball_instance = sphere;
				} 
				this.setListener();
			},
			setListener:function(){
				var _this = this;
				if(w_ctouch){
					/*$(w_ctouch).click(
						function(_evt){
							_this.listen(_evt);
						}
					);*/
					$(w_ctouch).mousedown(function(_evt){ 
						_mousedown();
					});
					$(w_ctouch).mouseup(function(_evt){ 
						_mouseup();
					});
					$(w_ctouch).mouseleave(function(_evt){ 
						_mouseup();
					});
					$(w_ctouch).mouseout(function(_evt){ 
						_mouseup();
					});
				} 
				
			}, 
			listen:function(evt){   
				if(w_wgl.camera){
					var _dir = w_wgl.camera.getWorldDirection();
					var _pos = w_wgl.camera.position;
					var _ball = undefined;
					var _ball_find = false;
					for(var _idx in _lib_shooter_balls){
						_ball = _lib_shooter_balls[_idx];
						if(!_ball.visible){
							_ball_find = true;
							break;
						}
					}

					if(!_ball_find){
						_ball = _ball_instance.clone(); 
						_lib_shooter_balls.push(_ball);
						w_wgl.scene.add(_ball);
					} 
					_ball._cin1 = {};
					_ball._cin1.v0 = 700/1000;
					_ball._cin1.v0_def = 700/1000;
					_ball._cin1.vf = 0;
					_ball._cin1.vf_def = 0;
					_ball._cin1.acc = 900/1000;
					_ball.visible = true;
					_ball.position.copy(_pos).add(_dir.clone().normalize().multiplyScalar(_ball_distance_camera));
					_ball._lib_last_position = new THREE.Vector3();
					_ball._lib_last_position.copy(_ball.position);
					_ball._dir = _dir.clone().normalize();   

				}
			}, 
			render:function(){ 
				for(var _idx in _lib_shooter_balls){
					var _ball = _lib_shooter_balls[_idx];
					if(_ball.visible){
						_ball._lib_last_position.copy(_ball.position);
						_ball.position.x +=_ball._dir.x*_ball._cin1.vf;
						_ball.position.y +=_ball._dir.y*_ball._cin1.vf;
						_ball.position.z +=_ball._dir.z*_ball._cin1.vf;
						_ball._cin1.vf = _ball._cin1.v0 + _ball._cin1.acc*_dt;
						_ball._cin1.v0 = _ball._cin1.vf;

						if(_ball.position.distanceTo(w_wgl.camera.position)>100 || _ball.position.y<0){
							_ball.visible = false;
						}
					} 
					if(!_ball.visible)continue;
				}
			}, 
			checkCollisions:function(_ball){  
				//this.checkCollisionsWithGlobos(_ball);
			}, 
			line3SphereIntersecction:function(line3,obj){
				var _r2 = 0;
				var _o = line3.start;
				var _c = 0;
				var _l = line3._lib_vunit;
				if(obj instanceof THREE.Sphere){
					_r2 = Math.pow(sphere.radius,2);
					_c = sphere.center.clone();
				}else if(obj instanceof THREE.Mesh){
					_r2 = Math.pow(obj.geometry.boundingSphere.radius,2);
					_c = obj.geometry.boundingSphere.center.clone().add(obj.position);
				}else{
					return false;
				}

				var _oc1 = _c.clone().sub(_o);
				_oc1 = Math.pow(_oc1.x,2)+Math.pow(_oc1.y,2)+Math.pow(_oc1.z,2);

				var _oc2 = _c.clone().sub(_o);
				_oc2 = _oc2.dot(_l);
				_oc2 = Math.pow(_oc2,2);

				var _result = _oc2 - _oc1 + _r2;
				if(_result>=0)return true;
			},
			sphereMeshCollisionSphere:function(sphere,mesh){
				return mesh.geometry.boundingSphere && sphere.center.distanceTo(mesh.position)<=(sphere.radius+mesh.geometry.boundingSphere.radius);
			},
			meshMeshCollisionSphere:function(mesh1,mesh2){ 
				if(
					mesh1.geometry.boundingSphere && 
					mesh2.geometry.boundingSphere 
				){
					var _sum = mesh1.geometry.boundingSphere.radius + mesh2.geometry.boundingSphere.radius;
					return mesh1.position.distanceTo(mesh2.position) <= _sum;
				}
				return false;
			},
			line3IntersectPlane:function(l_point,l_dir,p_normal,p_constant,p_point){
				var _dot1 = l_dir.dot(p_normal);
				var _dot2 = p_point.clone().sub(l_point).dot(p_normal);
				if(_dot1<_aproxZero && _dot1>-_aproxZero){//casi cero.
					if(_dot2<_aproxZero && _dot2>-_aproxZero){//casi cero.
						// linea contenida en el plano
						return 1;
					}else{
						// linea y plano no se intersectan.
						return 0;
					}
				}else{
					var _d = _dot2/_dot1;
					return l_dir.clone().multiplyScalar(_d).add(l_point);
				}
			},
			line3IntersectSphere:function(l_point,l_dir,s_center,s_radius){ 
				var _oc = l_point.clone().sub(s_center);
				var _disc = Math.pow(_oc.dot(l_dir),2) - _oc.lengthSq() + Math.pow(s_radius,2);
				//x = o + dl;
				var _dot1 = _oc.dot(l_dir);
				if(_disc<0){
					//no hay soluciÃ³n
					return null;
				}else if(_disc==0){
					//una soluciÃ³n
					return l_dir.clone().multiplyScalar(-_dot1).add(l_point);
				}else{
					var _disc_sqrt = Math.sqrt(_disc);
					var _d1 = -_dot1 + _disc_sqrt;
					var _d2 = -_dot1 - _disc_sqrt;
					return new THREE.Line3(
						l_dir.clone().multiplyScalar(_d2).add(l_point),
						l_dir.clone().multiplyScalar(_d1).add(l_point)
					);
				}
			},
			resize:function(){ 
			} 
		};

		return {
			instance:function(_p){
				if(!_instance){
					_instance = new Shooter(_p);
					return _instance;
				}
				return null;
			}
		};
	}
)();