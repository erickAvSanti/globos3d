var appgame = appgame || {};

appgame.Bloques = (
	function(){

		var _instance = null;  

		var _loader = new THREE.JSONLoader();  
		
		Bloques = function(){
		};
		Bloques.prototype = {
			constructor:Bloques, 
			test:function(){
				console.log("hello from Bloques");
			},
			start:function(){  
				var _this = this; 
				this.setListener();
			}, 
			setListener:function(){
			}, 
			listen:function(evt){ 
			}, 
			render:function(){ 
			}, 
			resize:function(){ 
			} 
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new Bloques();
					return _instance;
				}
				return null;
			}
		};
	}
)();

var appgame = appgame || {};

appgame.Shooter = (
	function(){

		var _instance = null;  

		window._lib_shooter_balls = [];
		var _ball_velocity = 50/1000;

		var _ball_instance = undefined;

		var _ball_distance_camera = 2;

		var _dt = 20/1000;

		var _parent = undefined;//CameraControl

		var _aproxZero = 0.00001;
 

		var _interval_mousedown_id = -1;

		var _mousedown = function(){
			if(_interval_mousedown_id==-1){
				_interval_mousedown_id = setInterval(_instance.listen, 100);
			}
     		
		};
		var _mouseup = function(){
			if(_interval_mousedown_id!=-1){
		     	clearInterval(_interval_mousedown_id);
		     	_interval_mousedown_id=-1;
		   	}
		};

		Shooter = function(_p){
			_parent = _p;
		};
		Shooter.prototype = {
			constructor:Shooter, 
			test:function(){
				console.log("hello from Shooter");
			},
			start:function(){  
				if(w_wgl.scene){
					var geometry = new THREE.SphereGeometry( 0.2, 8, 8 );
					var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
					material.opacity = 0.2;
					material.transparent = true;
					var sphere = new THREE.Mesh( geometry, material ); 
					_ball_instance = sphere;
				} 
				this.setListener();
			},
			setListener:function(){
				return;
				var _this = this;
				if(w_ctouch){ 
					$(w_ctouch).mousedown(function(_evt){ 
						_mousedown();
					});
					$(w_ctouch).mouseup(function(_evt){ 
						_mouseup();
					});
					$(w_ctouch).mouseleave(function(_evt){ 
						_mouseup();
					});
					$(w_ctouch).mouseout(function(_evt){ 
						_mouseup();
					});
				} 
				
			}, 
			listen:function(evt){   
				if(w_wgl.camera){
					var _dir = w_wgl.camera.getWorldDirection();
					var _pos = w_wgl.camera.position;
					var _ball = undefined;
					var _ball_find = false;
					for(var _idx in _lib_shooter_balls){
						_ball = _lib_shooter_balls[_idx];
						if(!_ball.visible){
							_ball_find = true;
							break;
						}
					}

					if(!_ball_find){
						_ball = _ball_instance.clone(); 
						_lib_shooter_balls.push(_ball);
						w_wgl.scene.add(_ball);
					} 
					_ball._cin1 = {};
					_ball._cin1.v0 = 700/1000;
					_ball._cin1.v0_def = 700/1000;
					_ball._cin1.vf = 0;
					_ball._cin1.vf_def = 0;
					_ball._cin1.acc = 900/1000;
					_ball.visible = true;
					_ball.position.copy(_pos).add(_dir.clone().normalize().multiplyScalar(_ball_distance_camera));
					_ball._lib_last_position = new THREE.Vector3();
					_ball._lib_last_position.copy(_ball.position);
					_ball._dir = _dir.clone().normalize();   

				}
			}, 
			render:function(){ 
				if(_lib_game_paused)return;
				for(var _idx in _lib_shooter_balls){
					var _ball = _lib_shooter_balls[_idx];
					if(_ball.visible){
						_ball._lib_last_position.copy(_ball.position);
						_ball.position.x +=_ball._dir.x*_ball._cin1.vf;
						_ball.position.y +=_ball._dir.y*_ball._cin1.vf;
						_ball.position.z +=_ball._dir.z*_ball._cin1.vf;
						_ball._cin1.vf = _ball._cin1.v0 + _ball._cin1.acc*_dt;
						_ball._cin1.v0 = _ball._cin1.vf;

						if(_ball.position.distanceTo(w_wgl.camera.position)>100 || _ball.position.y<0){
							_ball.visible = false;
						}
					} 
					if(!_ball.visible)continue;
				}
			}, 
			checkCollisions:function(_ball){  
				//this.checkCollisionsWithGlobos(_ball);
			}, 
			line3SphereIntersecction:function(line3,obj){
				var _r2 = 0;
				var _o = line3.start;
				var _c = 0;
				var _l = line3._lib_vunit;
				if(obj instanceof THREE.Sphere){
					_r2 = Math.pow(obj.radius,2);
					_c = obj.center.clone();
				}else if(obj instanceof THREE.Mesh){
					_r2 = Math.pow(obj.geometry.boundingSphere.radius,2);
					_c = obj.geometry.boundingSphere.center.clone().add(obj.position);
				}else{
					return false;
				}

				var _oc1 = _c.clone().sub(_o);
				_oc1 = Math.pow(_oc1.x,2)+Math.pow(_oc1.y,2)+Math.pow(_oc1.z,2);

				var _oc2 = _c.clone().sub(_o);
				_oc2 = _oc2.dot(_l);
				_oc2 = Math.pow(_oc2,2);

				var _result = _oc2 - _oc1 + _r2;
				if(_result>=0)return true;
				return false;
			},
			sphereMeshCollisionSphere:function(sphere,mesh){
				return mesh.geometry.boundingSphere && sphere.center.distanceTo(mesh.position)<=(sphere.radius+mesh.geometry.boundingSphere.radius);
			},
			meshMeshCollisionSphere:function(mesh1,mesh2){ 
				if(
					mesh1.geometry.boundingSphere && 
					mesh2.geometry.boundingSphere 
				){
					var _sum = mesh1.geometry.boundingSphere.radius + mesh2.geometry.boundingSphere.radius;
					return mesh1.position.distanceTo(mesh2.position) <= _sum;
				}
				return false;
			},
			line3IntersectPlane:function(l_point,l_dir,p_normal,p_constant,p_point){
				var _dot1 = l_dir.dot(p_normal);
				var _dot2 = p_point.clone().sub(l_point).dot(p_normal);
				if(_dot1<_aproxZero && _dot1>-_aproxZero){//casi cero.
					if(_dot2<_aproxZero && _dot2>-_aproxZero){//casi cero.
						// linea contenida en el plano
						return 1;
					}else{
						// linea y plano no se intersectan.
						return 0;
					}
				}else{
					var _d = _dot2/_dot1;
					return l_dir.clone().multiplyScalar(_d).add(l_point);
				}
			},
			line3IntersectSphere:function(l_point,l_dir,s_center,s_radius){ 
				var _oc = l_point.clone().sub(s_center);
				var _disc = Math.pow(_oc.dot(l_dir),2) - _oc.lengthSq() + Math.pow(s_radius,2);
				//x = o + dl;
				var _dot1 = _oc.dot(l_dir);
				if(_disc<0){
					//no hay soluciÃ³n
					return null;
				}else if(_disc==0){
					//una soluciÃ³n
					return l_dir.clone().multiplyScalar(-_dot1).add(l_point);
				}else{
					var _disc_sqrt = Math.sqrt(_disc);
					var _d1 = -_dot1 + _disc_sqrt;
					var _d2 = -_dot1 - _disc_sqrt;
					return new THREE.Line3(
						l_dir.clone().multiplyScalar(_d2).add(l_point),
						l_dir.clone().multiplyScalar(_d1).add(l_point)
					);
				}
			},
			resize:function(){ 
			} 
		};

		return {
			instance:function(_p){
				if(!_instance){
					_instance = new Shooter(_p);
					return _instance;
				}
				return null;
			}
		};
	}
)();

var appgame = appgame || {};

appgame.CameraBounding = (
	function(){

		var _instance = null;  

		var _boundingCylinder = {
			radius:0.3,
			minY:-1.5,
			maxY:1.5,
			center:null/*current camera position*/
		};

		var _halfPI = Math.PI/2;

		var _boundingSphere = {
			radius:Math.sqrt(Math.pow(_boundingCylinder.radius,2)+Math.pow(_boundingCylinder.maxY,2)),
			center:null/*current camera position*/
		}; 

		var _tmpv1 = new THREE.Vector3();
		var _tmpv2 = new THREE.Vector3();
		var _tmpv3 = new THREE.Vector3();

		var _up = new THREE.Vector3(0,1,0);

		var _cameraControl = undefined;

		CameraBounding = function(camCtrl){
			_cameraControl = camCtrl;
		};
		CameraBounding.prototype = {
			constructor:CameraBounding, 
			test:function(){
				console.log("hello from CameraBounding");
			},
			start:function(){ 
			}, 
			checkCollisions:function(){ 
			}, 
			composeVector3:function(v1,v2,scale){
				return new THREE.Vector3(
						v1.x*scale + v2.x * scale,
						v1.y*scale + v2.y * scale,
						v1.z*scale + v2.z * scale
					); 
			},
			obtenerVectorComponente:function(a,b){
				b = b.clone();
				b.normalize();
				var _scalar = a.x*b.x + a.y*b.y+a.z*b.z;
				return b.multiplyScalar(_scalar);
			},
			intersectionSphereSphere:function(s1_center,s1_radius,s2_center,s2_radius){
				return s1_center.distanceTo(s2_center)<=(s1_radius+s2_radius);
			}
		};

		return {
			instance:function(camCtrl){
				if(!_instance){
					_instance = new CameraBounding(camCtrl);
					return _instance;
				}
				return null;
			}
		};
	}
)();


var appgame = appgame || {};

appgame.CameraControl = (
	function(){

		var _instance = null;  

		var _btnChange = false;

		var _up = new THREE.Vector3(0,1,0);

		var _camPos = new THREE.Vector3();
		var _lookAt = new THREE.Vector3();
		var _lookAtDir = new THREE.Vector3();

		var _cameraYPosMin = 3;

		var _debug = false;

		var _dotScalar = 0;

		var _touch_identifier = null;
		var _touch_identifier_move_keys = null;
		var _touch_identifier_for_fire = null;

		var _touchCurrentPoint = new THREE.Vector2();
		var _touchLastPoint = new THREE.Vector2();

		var _keyA,_keyS,_keyW,_keyD,_keyN,_keySpace=false;
		  

		var _vFront_def = 90/1000;
		var _vRight_def = _vFront_def;

		var _v0Front = _vFront_def;
		var _vfFront = _vFront_def;
		var _accFront = 50/1000;

		var _v0Right = _vRight_def;
		var _vfRight = _vRight_def;
		var _accRight = 50/1000;

 
		var _dt = 20/1000;

		var _a360 = 2*Math.PI;
		var _radCircle = 33;
		var _radCircleExt = _radCircle-5;
		var _radMasterCircle = 90;
		var _points = {
			left:{x:0,y:0},			
			top:{x:0,y:0},			
			bottom:{x:0,y:0},			
			right:{x:0,y:0},
			fire:{x:0,y:0},
			x:0,
			y:0,
			touchedX:0,			
			touchedY:0			
		};
		window._lib_touchedForFire = false;

		var _tmpV2_p1 = new THREE.Vector2();
		var _tmpV2_p2 = new THREE.Vector2();

		var _supportsTouch = null;

		var _jump = {
			running:false,
			v0:0,
			v0_def:-200/1000,
			vf:0,
			vf_def:0,
			acc:0,
			acc_def:500/1000,
			last_acc:0
		};
 

		var _shooter = undefined;

		var _cameraBounding = undefined;

		var _start_running_gravity = false;

		var _debug_console = false;


		var _cameraAngle_def = 60;
		var _cameraAngle = _cameraAngle_def;

		var _zoom_value = 1;
 
 		var _limitXY = 35;

 		window._lib_slice = 1.5;

		CameraControl = function(){

		};
		CameraControl.prototype = {
			constructor:CameraControl, 
			test:function(){
				console.log("hello from CameraControl");
			},
			supportsTouch:function(){
				if(_supportsTouch===null){
					_supportsTouch = "ontouchstart" in window;
				}
				return _supportsTouch;
			},
			start:function(){  
				if ("onpointerlockchange" in document) {

					var _this = this;
					$(window).keyup(function(_evt){_this.listenKeyUp(_evt);});  
					$(window).keydown(function(_evt){_this.listenKeyDown(_evt);});   

					$(w_ctouch).click(
						function(_evt){
							if( 
								!w_ctouch._lib_fullscreen_clicked && 
								!w_ctouch._lib_play_pause_clicked
							){
								_this.touchClick(_evt);
							} 
						}
					);
					$(w_ctouch).mousewheel(function(_evt) {
					    _this.listenWhell(_evt);
					});
					(_cameraBounding = appgame.CameraBounding.instance(this)).start();

					document.addEventListener('pointerlockchange', _this.changeLock, false);
					document.addEventListener('mozpointerlockchange', _this.changeLock, false);
					document.addEventListener('webkitpointerlockchange', _this.changeLock, false);
					document.addEventListener('pointerlockerror', _this.errorLock, false);
					document.addEventListener('mozpointerlockerror', _this.errorLock, false);
					document.addEventListener('webkitpointerlockerror', _this.errorLock, false);

					document.addEventListener("mousemove",_this.mouseMove, false);
				} 
				this.setMinorHeight();
				this.calcPoints();

				(_shooter = appgame.Shooter.instance(this)).start(); 
				$( "#master_hand" ).on( "slidechange",function(event, ui){
					var _val = ui.value/100;
					_lib_slice = 1+_val*2;
				});

			}, 
			listenWhell:function(_evt){
				//console.log(event.deltaX, event.deltaY, event.deltaFactor);
				if(_evt.deltaY>0){
					_zoom_value++;
				}else{
					_zoom_value--;
				}
				if(_zoom_value<1)_zoom_value = 1;
				if(_zoom_value>25)_zoom_value = 25; 
			},
			touchClick:function(_evt){
				if(!this.hasPointerLock()){
					this.requestPointerLock();
				}
			},
			changeLock:function(_evt){
				var _this = this; 
			},
			errorLock:function(_evt){

			},
			resize:function(){
				this.setMinorHeight();
				this.calcPoints();
			},
			setMinorHeight:function(){ 
				if(w_height<=300){
					_radCircle = 33-5;
					_radCircleExt = _radCircle-5;
					_radMasterCircle = 90-5;
				}else{ 
					_radCircle = 33;
					_radCircleExt = _radCircle-5;
					_radMasterCircle = 90;
				} 
			},
			calcPoints:function(){ 
				_points.top.x = 120;
				_points.top.y = w_height-170;
				_points.bottom.x = 120;
				_points.bottom.y = w_height-70; 
				_points.left.x = 70; 
				_points.left.y = w_height-120; 
				_points.right.x = 170; 
				_points.right.y = w_height-120; 
				_points.fire.x = 45; 
				_points.fire.y = w_height-240; 
				_points.x = 120; 
				_points.y = w_height-120; 

				if(w_height<=300){ 
					_points.top.y +=30;
					_points.bottom.y +=30;
					_points.left.y +=30;
					_points.right.y +=30;
					_points.fire.y +=40;
					_points.y +=30;
				} 

			},
			mouseMove:function(_evt){
				if(_instance.hasPointerLock()){
				  	var x = _evt.movementX;
				  	var y = _evt.movementY; 
				  	_instance.rotView(x,y);
				}
			},
			listenKeyUp:function(_evt){
				var _keyCode = _evt.which || _evt.keyCode;  
				if(_keyCode==75){//keyK
					_btnChange = !_btnChange;
				}   
				if(_keyCode==78){//keyN
					_keyN = !_keyN;
				}   
				if(_keyCode==87)_keyW = false;
				if(_keyCode==83)_keyS = false;
				if(_keyCode==68)_keyD = false;
				if(_keyCode==65)_keyA = false;  
			}, 
			listenKeyDown:function(_evt){
				var _keyCode = _evt.which || _evt.keyCode;  
				if(_keyCode==87)_keyW = true;
				if(_keyCode==83)_keyS = true;
				if(_keyCode==68)_keyD = true;
				if(_keyCode==65)_keyA = true; 
				if(_keyCode==32)_keySpace = true;
			},
			obtenerVectorComponente:function(a,b){
				b = b.clone();
				b.normalize();
				var _scalar = a.x*b.x + a.y*b.y+a.z*b.z;
				return b.multiplyScalar(_scalar);
			},
			render:function(){
				if(_btnChange || window._lib_force_exit_pointer_lock){
					this.exitPointerLock();
					_btnChange = false;
					window._lib_force_exit_pointer_lock = false;
				}  
				if(_touch_identifier!==null){
					if(
						_touchCurrentPoint.x!=0 && 
						_touchCurrentPoint.y!=0 && 
						!_touchCurrentPoint.equals(_touchLastPoint)
					){
						_instance.rotView(
							(_touchCurrentPoint.x - _touchLastPoint.x)*_lib_slice,
							(_touchCurrentPoint.y - _touchLastPoint.y)*_lib_slice
						);
					}
				} 
				if(_zoom_value!=w_wgl.camera.zoom){
					w_wgl.camera.zoom = _zoom_value;
					w_wgl.camera.updateProjectionMatrix();
				}
				this.translate();
				if(_cameraBounding){
					_cameraBounding.checkCollisions(); 
				}
				if(w_wgl.camera.position.x>_limitXY)w_wgl.camera.position.x = _limitXY;
				if(w_wgl.camera.position.x<-_limitXY)w_wgl.camera.position.x = -_limitXY;
				if(w_wgl.camera.position.z>_limitXY)w_wgl.camera.position.z = _limitXY;
				if(w_wgl.camera.position.z<-_limitXY)w_wgl.camera.position.z = -_limitXY;
				if(this.supportsTouch() || _keyN){
					w_ctouch_ctx.fillStyle = 'rgba(255,255,255,0.1)';
					w_ctouch_ctx.strokeStyle = 'rgba(0,0,0,0.3)';
					w_ctouch_ctx.lineWidth = 2;
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.arc(_points.x,_points.y,_radMasterCircle,0,_a360);
					w_ctouch_ctx.fill();
					w_ctouch_ctx.stroke();
					w_ctouch_ctx.closePath();
					//-----------------------------
					//left
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.arc(_points.left.x,_points.left.y,_radCircle,0,_a360);
					w_ctouch_ctx.fill();
					w_ctouch_ctx.stroke();
					w_ctouch_ctx.closePath();
					//-----------------------------
					//top
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.arc(_points.top.x,_points.top.y,_radCircle,0,_a360);
					w_ctouch_ctx.fill();
					w_ctouch_ctx.stroke();
					w_ctouch_ctx.closePath();
					//-----------------------------
					//bottom
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.arc(_points.bottom.x,_points.bottom.y,_radCircle,0,_a360);
					w_ctouch_ctx.fill();
					w_ctouch_ctx.stroke();
					w_ctouch_ctx.closePath();
					//-----------------------------
					//right
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.arc(_points.right.x,_points.right.y,_radCircle,0,_a360);
					w_ctouch_ctx.fill();
					w_ctouch_ctx.stroke();
					w_ctouch_ctx.closePath();
					//----------------------------- 
					//fire
					if(_lib_touchedForFire){
						w_ctouch_ctx.fillStyle = 'rgba(0,0,255,0.4)';
					}else{ 
						w_ctouch_ctx.fillStyle = 'rgba(255,255,255,0.4)';
					}
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.arc(_points.fire.x,_points.fire.y,_radCircle,0,_a360);
					w_ctouch_ctx.fill();
					w_ctouch_ctx.stroke();
					w_ctouch_ctx.closePath();
					//----------------------------- 
					if(
						_points.touchedX!=0 && 
						_points.touchedY!=0
					){
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.arc(_points.touchedX,_points.touchedY,_radCircleExt,0,_a360);
						w_ctouch_ctx.fill();
						w_ctouch_ctx.stroke();
						w_ctouch_ctx.closePath();
					}
					
				}
				//-----------------------------
				w_ctouch_ctx.fillStyle = 'rgba(255,255,255,0.4)';  
				w_ctouch_ctx.beginPath();
				w_ctouch_ctx.rect(w_width/2-50,w_height/2-0.5,100,1);
				w_ctouch_ctx.fill(); 
				w_ctouch_ctx.closePath();
				//----------------------------- 
				w_ctouch_ctx.beginPath();
				w_ctouch_ctx.rect(w_width/2-0.5,w_height/2-50,1,100); 
				w_ctouch_ctx.fill(); 
				w_ctouch_ctx.closePath();
				//-----------------------------
				if(_shooter)_shooter.render();
			},
			translate:function(){

				if(!w_wgl.camera._lib_last_position){
					w_wgl.camera._lib_last_position = new THREE.Vector3();
				}
				w_wgl.camera._lib_last_position.copy(w_wgl.camera.position);

				var _movFront = 0;
				var _movRight = 0;

				if(_keyW)_movFront+=1;
				if(_keyS)_movFront+=-1;
				if(_keyD)_movRight+=1;
				if(_keyA)_movRight+=-1;
 
				if( _keySpace && _jump.acc==0 ){ 
					_jump.v0 = _jump.v0_def;   
					_jump.acc = _jump.acc_def;  
				}  
				this.applyGravity();  
				_keySpace = false; 
				if(_movFront!=0 || _movRight!=0){

					var _wdir = w_wgl.camera.getWorldDirection().clone().normalize();
					if(_movRight!=0){  
						if(_vfRight<1)_vfRight = _v0Right + _accRight*_dt;    
						var _wrightDir = _wdir.clone().cross(_up).normalize();
						_wrightDir.multiplyScalar(_movRight*_vfRight);
						_v0Right = _vfRight;
						w_wgl.camera.position.add(_wrightDir); 
					}else{
						_vfRight = _v0Right = _vRight_def;
					}
					if(_movFront!=0){  
						if(_vfFront<1)_vfFront = _v0Front + _accFront*_dt;  
						//-------------------------------------------------- 
						var _tmpWdir = this.obtenerVectorComponente(_wdir,_up);
						_wdir.sub(_tmpWdir).normalize(); 
						//--------------------------------------------------
						_wdir.multiplyScalar(_movFront*_vfFront);
						_v0Front = _vfFront; 
						w_wgl.camera.position.add(_wdir); 
					}else{
						_vfFront = _v0Front = _vFront_def;
					}
				}else{
					_vfFront = _v0Front = _vFront_def;
					_vfRight = _v0Right = _vRight_def;
				}
				window.___vfFront = _vfFront;
				window.___v0Front = _v0Front;
				window.___vfRight = _vfRight;
				window.___v0Right = _v0Right;
				window.___v0 = _jump.v0;
				window.___vf = _jump.vf;
				window.___acc = _jump.acc;
				window.___last_acc = _jump.last_acc; 
			}, 
			applyGravity:function(){ 
				this.checkFloorPosition();
				if(_jump.acc!=0){ 
					_jump.vf = _jump.v0 + _jump.acc * _dt; 
					_jump.v0 = _jump.vf;  
					w_wgl.camera.position.sub(_up.clone().multiplyScalar(_jump.vf));
					this.checkFloorPosition();
				}   
			},
			checkFloorPosition:function(){ 
				if(w_wgl.camera.position.y<_cameraYPosMin){ 
					w_wgl.camera.position.y = _cameraYPosMin;  
					_jump.v0 = 0;
					_jump.acc = 0;  
				}  
			},
			startRunningGravity:function(){
				if(!_start_running_gravity){ 
					this.startGravity(_start_running_gravity = true);
				} 
			},
			disableRunningGravity:function(prevent){
				_start_running_gravity = false; 
				if(prevent){
					if(_jump.v0>=0)this.setV0VfZero();
				}else{
					this.setV0VfZero();
				} 
			},
			resetAcc:function(){  
				if(w_wgl.camera.position.y<=_cameraYPosMin){
					this.setAccZero();
				}else{
					_jump.acc = _jump.acc_def;
				} 
			},
			setAccZero:function(){  
				_jump.acc = 0;
			},
			setV0Zero:function(){ 
				_jump.v0 = 0;
			},
			setVfZero:function(){ 
				_jump.vf = 0;
			},
			resetV0:function(){ 
				_jump.v0 = _jump.v0_def;
			},
			resetVf:function(){ 
				_jump.vf = _jump.vf_def;
			},
			setV0VfZero:function(){ 
				this.setV0Zero();
				this.setVfZero();
			},
			resetVFrontVRightZero:function(){ 
				_vfFront = _v0Front = _vFront_def;
				_vfRight = _v0Right = _vRight_def;
			},
			startGravity:function(force){
				if(_jump.v0 == 0 || (force && _jump.v0>=0))_jump.v0 = 0.000001; 
			},
			requestPointerLock:function(){
				if(!w_ctouch.requestPointerLock){
					w_ctouch.requestPointerLock = 
						w_ctouch.requestPointerLock || 
						w_ctouch.mozRequestPointerLock || 
						w_ctouch.webkitRequestPointerLock;
				} 
				if(w_ctouch.requestPointerLock){
					w_ctouch.requestPointerLock();
					console.log("solicitando bloqueo de document.");	
				} 
			},
			exitPointerLock:function(){
				if(!document.exitPointerLock){
					document.exitPointerLock = 
						document.exitPointerLock || 
						document.mozExitPointerLock || 
						document.webkitExitPointerLock;
				} 
				if(document.exitPointerLock){
					document.exitPointerLock();
					console.log("retirando bloqueo de document.");
				} 
			},
			hasPointerLock:function(){
				if(!document.pointerLockElement){
					document.pointerLockElement = 
						document.pointerLockElement || 
						document.mozPointerLockElement || 
						document.webkitPointerLockElement;
					} 
				return w_ctouch && document.pointerLockElement == w_ctouch;
			},
			collidePoints2D:function(x1,y1,x2,y2,_radius){
				_tmpV2_p1.set(x1,y1);
				_tmpV2_p2.set(x2,y2); 
				if(_tmpV2_p1.distanceTo(_tmpV2_p2)<=_radius){ 
					return true;
				} 
			},
			rotView:function(x,y){
				if(x!=0 || y!=0){
					var _frontDir = w_wgl.camera.getWorldDirection().clone().normalize();
					var _rightDir = _frontDir.clone().cross(_up).normalize(); 
					_camPos.copy(w_wgl.camera.position);
					_lookAt.set(0,0,0);
					if(_debug){
						console.log("_frontDir",_frontDir);
						console.log("_rightDir",_rightDir);
						console.log("_camPos",_camPos); 
					}
			        if(x!=0)this.rotViewAround(-x/360.0,_up,_frontDir);
			        if(y!=0)this.rotViewAround(-y/360.0,_rightDir,_frontDir); 
					if(_debug){
						console.log("_lookAt",_lookAt); 
					}
					_lookAtDir.copy(_lookAt).normalize();
			        _lookAt.add(_camPos);
					if(_debug){
						console.log("_lookAt2",_lookAt); 
					}
					_dotScalar = _up.dot(_lookAtDir);
					if(_dotScalar>=-0.995 && _dotScalar<=0.995){
						w_wgl.camera.lookAt(_lookAt);
					} 
				} 
			}, 
			rotViewAround:function(deltaAngle, axis,vecRot) {
				var _axis = vec3.create([axis.x,axis.y,axis.z]); 
				var _vecRot = vec3.create([vecRot.x,vecRot.y,vecRot.z]); 
		        var q = quat4.create();
		        quat4.fromAngleAxis(deltaAngle,_axis, q);
		        quat4.multiplyVec3(q,_vecRot);
		        vec3.normalize(_vecRot);
				if(_debug){
					console.log("_vecRot",_vecRot); 
				}
		       	_lookAt.x += _vecRot[0];
		       	_lookAt.y += _vecRot[1];
		       	_lookAt.z += _vecRot[2];  
		    },
			touch:function(_what,_evt){

				if( 
					!w_ctouch._lib_fullscreen_clicked && 
					!w_ctouch._lib_play_pause_clicked && 
					this.supportsTouch() 
				){
					if(_what=="s")this.touch_start(_evt);
					if(_what=="e")this.touch_end(_evt);
					if(_what=="c")this.touch_cancel(_evt);
					if(_what=="l")this.touch_leave(_evt);
					if(_what=="m")this.touch_move(_evt);
				}  
			},
			touch_start:function(_evt){ 
				if(
					_touch_identifier===null || 
					_touch_identifier_move_keys===null || 
					_touch_identifier_for_fire===null
				){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(
							this.collidePoints2D(
								_touch.clientX,
								_touch.clientY,
								_points.x,
								_points.y,
								_radMasterCircle
							)
						){
							if(_touch_identifier_move_keys===null){ 
								_touch_identifier_move_keys = _touch.identifier;
								this.handleTouchMoveKey(_touch.clientX,_touch.clientY);
								break;
							}
						}
						else if(
							this.collidePoints2D(
								_touch.clientX,
								_touch.clientY,
								_points.fire.x,
								_points.fire.y,
								_radMasterCircle
							)
						){
							if(_touch_identifier_for_fire===null){ 
								_touch_identifier_for_fire = _touch.identifier;
								_lib_touchedForFire = true;
								break;
							}
						}else{
							if(_touch_identifier===null){
								_touch_identifier = _touch.identifier;
								_touchCurrentPoint.set(_touch.clientX,_touch.clientY);
								_touchLastPoint.copy(_touchCurrentPoint);
								break;
							}
						}  
						
					} 
				} 
			},
			touch_move:function(_evt){  
				if(
					_touch_identifier!==null || 
					_touch_identifier_move_keys!==null || 
					_touch_identifier_for_fire!==null
				){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(_touch_identifier!==null && _touch_identifier==_touch.identifier){ 
							_touchLastPoint.copy(_touchCurrentPoint);
							_touchCurrentPoint.set(_touch.clientX,_touch.clientY);
							break;
						}
						if(_touch_identifier_move_keys!==null && _touch_identifier_move_keys==_touch.identifier){ 
							this.handleTouchMoveKey(_touch.clientX,_touch.clientY);
							break;
						} 
						if(_touch_identifier_for_fire!==null && _touch_identifier_for_fire==_touch.identifier){ 
							//this.handleTouchMoveKey(_touch.clientX,_touch.clientY);
							break;
						} 
					} 
				} 
			},
			touch_end:function(_evt){  
				if(
					_touch_identifier!==null || 
					_touch_identifier_move_keys!==null || 
					_touch_identifier_for_fire!==null
				){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx]; 
						if(_touch_identifier!==null && _touch_identifier==_touch.identifier){ 
							_touch_identifier = null;
							_touchLastPoint.copy(_touchCurrentPoint);
							break;
						}
						if(_touch_identifier_move_keys!==null && _touch_identifier_move_keys==_touch.identifier){ 
							_touch_identifier_move_keys = null; 
							_keyW = false;
							_keyA = false;
							_keyS = false;
							_keyD = false;
							break;
						}
						if(_touch_identifier_for_fire!==null && _touch_identifier_for_fire==_touch.identifier){ 
							_touch_identifier_for_fire = null; 
							_lib_touchedForFire = false;
							break;
						}
					} 
				}
				_points.touchedX=_points.touchedY = 0;  
			},
			touch_cancel:function(_evt){  
				this.touch_end(_evt);
			},
			touch_leave:function(_evt){  
				this.touch_end(_evt);
			},
			handleTouchMoveKey:function(x,y){ 
				if(
					this.collidePoints2D(
						x,
						y,
						_points.x,
						_points.y,
						_radMasterCircle
					)
				){
					_points.touchedX = x;
					_points.touchedY = y;
					if(
						this.collidePoints2D(
							x,
							y,
							_points.top.x,
							_points.top.y,
							_radCircleExt*2
						)
					){
						_keyW = true;
					}else{
						_keyW = false;
					}
					if(
						this.collidePoints2D(
							x,
							y,
							_points.bottom.x,
							_points.bottom.y,
							_radCircleExt*2
						)
					){
						_keyS = true;
					}else{
						_keyS = false;
					}
					if(
						this.collidePoints2D(
							x,
							y,
							_points.left.x,
							_points.left.y,
							_radCircleExt*2
						)
					){
						_keyA = true;
					}else{
						_keyA = false;
					}
					if(
						this.collidePoints2D(
							x,
							y,
							_points.right.x,
							_points.right.y,
							_radCircleExt*2
						)
					){
						_keyD = true;
					}else{
						_keyD = false;
					}

				}else{
					_keyW = false;
					_keyS = false;
					_keyD = false;
					_keyA = false;
					_points.touchedX = 0;
					_points.touchedY = 0;
				}
			}
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new CameraControl();
					return _instance;
				}
				return null;
			}
		};
	}
)();

var appgame = appgame || {};

appgame.PlayPause = (
	function(){

		var _instance = null; 

		var _btn_pause_src = "./imgs/boton_pause.png";
		var _btn_play_src = "./imgs/boton_play.png";

		var _btn_pause = null;
		var _btn_play = null;  

		var _width = 90;
		var _height = 90;

		var _radius = 30;

		var _width2 = _radius*2;
		var _height2 = _radius*2;

		var _point = {x:10,y:10,offsetX:10,offsetY:100}; 
		
		var _touch_identifier = null;

		window._lib_game_paused = true;

		PlayPause = function(){

		};
		PlayPause.prototype = {
			constructor:PlayPause, 
			test:function(){
				console.log("hello from PlayPause");
			},
			start:function(){
				this.cargarSprites();
				this.configurarPosicionSprites();
				this.setListener();
			},
			setListener:function(){
				var _this = this;
				if(w_ctouch){
					$(w_ctouch).click(
						function(_evt){
							_this.listen(_evt);
							_evt.preventDefault();
						}
					);
				} 
				
			}, 
			listen:function(evt){ 
				var _v2a = new THREE.Vector2(_point.x+_width2/2,_point.y+_height2/2);
				var _v2b = new THREE.Vector2(evt.clientX,evt.clientY);
				if(_v2a.distanceTo(_v2b)<=_radius){  
					w_ctouch._lib_play_pause_clicked = true;
					_lib_game_paused = !_lib_game_paused;
				}else{
					w_ctouch._lib_play_pause_clicked = false;
				}
			}, 
			render:function(){
				this.dibujarSprites();
				w_ctouch._lib_play_pause_clicked = false;
			}, 
			resize:function(){
				this.configurarPosicionSprites();
			},
			configurarPosicionSprites:function(){
				_point.x = w_width - _width2 - _point.offsetX;
				_point.y = _point.offsetY;
			},
			cargarSprites:function(){
				if(!w_ctouch_ctx)return; 
				_btn_pause = new Image;
				_btn_pause.onload=function(){
					_btn_pause._loaded = true;
				};	
				_btn_pause.src = _btn_pause_src;
				_btn_play = new Image; 
				_btn_play.onload=function(){
					_btn_play._loaded = true;
				};	
				_btn_play.src = _btn_play_src; 
			},
			dibujarSprites:function(){ 
				if(_lib_game_paused){
					if(w_ctouch_ctx && _btn_play && _btn_play._loaded){  
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.drawImage(_btn_play,0,0,_width,_height,_point.x,_point.y,_width2,_height2);
						w_ctouch_ctx.closePath();
					} 
				}else{
					if(w_ctouch_ctx && _btn_pause && _btn_pause._loaded){  
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.drawImage(_btn_pause,0,0,_width,_height,_point.x,_point.y,_width2,_height2);
						w_ctouch_ctx.closePath();
					} 	
				} 
			},
			touch:function(_what,_evt){
				return;
				if(_what=="s")this.touch_start(_evt);
				if(_what=="e")this.touch_end(_evt);
				if(_what=="c")this.touch_cancel(_evt);
				if(_what=="l")this.touch_leave(_evt);
				if(_what=="m")this.touch_move(_evt);
			},
			touch_point_collide:function(_touch){
				if(_touch.clientX && _touch.clientY){
					var _v2a = new THREE.Vector2(_point.x+_width2/2,_point.y+_height2/2);
					var _v2b = new THREE.Vector2(_touch.clientX,_touch.clientY);
					if(_v2a.distanceTo(_v2b)<=_radius){ 
						return true;
					}
				}
				return false;
			},
			touch_start:function(_evt){ 
				if(_touch_identifier===null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(this.touch_point_collide(_touch)){
							//_touch_play = true;
							_touch_identifier = _touch.identifier;
							break;
						}
					} 
				} 
			},
			touch_move:function(_evt){  
				if(_touch_identifier!==null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(
							_touch_identifier==_touch.identifier 
						){
							if(this.touch_point_collide(_touch)){
								//_touch_play = true; 
							}else{
								//_touch_play = false;
							}
							break;
						} 
					} 
				} 
			},
			touch_end:function(_evt){  
				if(_touch_identifier!==null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx]; 
						if(_touch_identifier==_touch.identifier){
							//_touch_play = false;
							_touch_identifier = null;
							break;
						}
					} 
				}  
			},
			touch_cancel:function(_evt){  
				this.touch_end(_evt);
			},
			touch_leave:function(_evt){  
				this.touch_end(_evt);
			}
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new PlayPause();
					return _instance;
				}
				return null;
			}
		};
	}
)();


var appgame = appgame || {};

appgame.FullScreen = (
	function(){

		var _instance = null; 

		var _btn_maximize_src = "./imgs/boton_maximize.png";
		var _btn_minimize_src = "./imgs/boton_minimize.png";

		var _btn_maximize = null;
		var _btn_minimize = null;  

		var _width = 90;
		var _height = 90;

		var _radius = 30;

		var _width2 = _radius*2;
		var _height2 = _radius*2;

		var _point = {x:10,y:10,offsetX:10,offsetY:10};

		var _fullscreen_btn_clicked = false;
		
		var _touch_identifier = null;

		FullScreen = function(){

		};
		FullScreen.prototype = {
			constructor:FullScreen, 
			test:function(){
				console.log("hello from FullScreen");
			},
			start:function(){
				this.cargarSprites();
				this.configurarPosicionSprites();
				this.setListener();
			},
			setListener:function(){
				var _this = this;
				if(w_ctouch){
					$(w_ctouch).click(
						function(_evt){
							_evt.preventDefault();
							_this.listen(_evt);
						}
					);
				}
				window.addEventListener(
					"fullscreenchange", 
					function(evt){ 
						_this.onChange(evt);
					}
				);
				window.addEventListener(
					"webkitfullscreenchange", 
					function(evt){ 
						_this.onChange(evt);
					}
				);
				window.addEventListener(
					"mozfullscreenchange", 
					function(evt){ 
						_this.onChange(evt);
					}
				);
				window.addEventListener(
					"fullscreenerror", 
					function(evt){ 
						_this.onError(evt);
					}
				);
				window.addEventListener(
					"webkitfullscreenerror", 
					function(evt){ 
						_this.onError(evt);
					}
				);
				window.addEventListener(
					"mozfullscreenerror", 
					function(evt){ 
						_this.onError(evt);
					}
				);
				
			},
			onChange:function(evt){
				//console.log("change",evt);
				w_ctouch._lib_fullscreen_clicked = false;
			},
			onError:function(evt){
				//console.log("error",evt);
				w_ctouch._lib_fullscreen_clicked = false;
			},
			listen:function(evt){ 
				var _v2a = new THREE.Vector2(_point.x+_width2/2,_point.y+_height2/2);
				var _v2b = new THREE.Vector2(evt.clientX,evt.clientY);
				if(_v2a.distanceTo(_v2b)<=_radius){ 
					w_ctouch._lib_fullscreen_clicked = true;
					this.toggleFullScreen();  
				}else{
					w_ctouch._lib_fullscreen_clicked = false;
				}
			},
			toggleFullScreen:function() {
			  	if (!this.isWindowFullScreen()) {
			  		document.documentElement.requestFullscreen = 
			  			document.documentElement.webkitRequestFullscreen || 
			  			document.documentElement.mozRequestFullscreen || 
			  			document.documentElement.requestFullscreen;
			      	document.documentElement.requestFullscreen();
			  	}else{
			  		document.exitFullscreen = 
			  			document.exitFullscreen || 
			  			document.mozExitFullscreen || 
			  			document.webkitExitFullscreen;
			    	if (document.exitFullscreen) {
			      		document.exitFullscreen(); 
			    	}
			  	}
			},
			render:function(){
				this.dibujarSprites();
			},
			isWindowFullScreen:function(){
				return w_width == screen.width && w_height == screen.height;
			},
			resize:function(){
				this.configurarPosicionSprites();
			},
			configurarPosicionSprites:function(){
				_point.x = w_width - _width2 - _point.offsetX;
				_point.y = _point.offsetY;
			},
			cargarSprites:function(){
				if(!w_ctouch_ctx)return; 
				_btn_maximize = new Image;
				_btn_maximize.onload=function(){
					_btn_maximize._loaded = true;
				};	
				_btn_maximize.src = _btn_maximize_src;
				_btn_minimize = new Image; 
				_btn_minimize.onload=function(){
					_btn_minimize._loaded = true;
				};	
				_btn_minimize.src = _btn_minimize_src; 
			},
			dibujarSprites:function(){ 
				if(this.isWindowFullScreen()){
					if(w_ctouch_ctx && _btn_minimize && _btn_minimize._loaded){  
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.drawImage(_btn_minimize,0,0,_width,_height,_point.x,_point.y,_width2,_height2);
						w_ctouch_ctx.closePath();
					} 
				}else{
					if(w_ctouch_ctx && _btn_maximize && _btn_maximize._loaded){  
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.drawImage(_btn_maximize,0,0,_width,_height,_point.x,_point.y,_width2,_height2);
						w_ctouch_ctx.closePath();
					} 	
				} 
			},
			touch:function(_what,_evt){
				if(_what=="s")this.touch_start(_evt);
				if(_what=="e")this.touch_end(_evt);
				if(_what=="c")this.touch_cancel(_evt);
				if(_what=="l")this.touch_leave(_evt);
				if(_what=="m")this.touch_move(_evt);
			},
			touch_point_collide:function(_touch){
				if(_touch.clientX && _touch.clientY){
					var _v2a = new THREE.Vector2(_point.x+_width2/2,_point.y+_height2/2);
					var _v2b = new THREE.Vector2(_touch.clientX,_touch.clientY);
					if(_v2a.distanceTo(_v2b)<=_radius){ 
						return true;
					}
				}
				return false;
			},
			touch_start:function(_evt){ 
				if(_touch_identifier===null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(this.touch_point_collide(_touch)){
							_touch_play = true;
							_touch_identifier = _touch.identifier;
							break;
						}
					} 
				} 
			},
			touch_move:function(_evt){  
				if(_touch_identifier!==null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(
							_touch_identifier==_touch.identifier 
						){
							if(this.touch_point_collide(_touch)){
								_touch_play = true; 
							}else{
								_touch_play = false;
							}
							break;
						} 
					} 
				} 
			},
			touch_end:function(_evt){  
				if(_touch_identifier!==null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx]; 
						if(_touch_identifier==_touch.identifier){
							_touch_play = false;
							_touch_identifier = null;
							break;
						}
					} 
				}  
			},
			touch_cancel:function(_evt){  
				this.touch_end(_evt);
			},
			touch_leave:function(_evt){  
				this.touch_end(_evt);
			}
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new FullScreen();
					return _instance;
				}
				return null;
			}
		};
	}
)();


var appgame = appgame || {};

appgame.ZonaA = (
	function(){

		var _instance = null;
		var _loader = new THREE.JSONLoader();

		var _elements = {}; 
		_elements["piso"] = "./modelos/piso.json"; 
		_elements["nube_front"] = "./modelos/nube_front.json"; 
		_elements["nube_tras"] = "./modelos/nube_tras.json"; 
		_elements["nube_izq"] = "./modelos/nube_izq.json"; 
		_elements["nube_der"] = "./modelos/nube_der.json"; 
		_elements["bloque_front"] = "./modelos/bloque_front.json"; 
		_elements["bloque_tras"] = "./modelos/bloque_tras.json"; 
		_elements["malla_der"] = "./modelos/malla_der.json"; 
		_elements["malla_izq"] = "./modelos/malla_izq.json"; 
		_elements["globo"] = "./modelos/globo.json"; 

		window._lib_globos = [];
		var _globo_materials = [];
		var _mat_index = 0;

		var _globo_master = null;

		var _interval_add_id = null;

		window._sounds_ogg = [];
		window._sounds_m4a = [];  

		var _audioLoader = new THREE.AudioLoader();

		var _elapsed_time_globo_def = 1500;
		var _elapsed_time_globo = _elapsed_time_globo_def;
		var _elapsed_time_globo_min_facil = 900;
		var _elapsed_time_globo_min_medio = 750;
		var _elapsed_time_globo_min_dificil = 500;
		var _elapsed_time_globo_dec_facil = 10;
		var _elapsed_time_globo_dec_medio = 50;
		var _elapsed_time_globo_dec_dificil = 200;

		var _contador_globos_reventados = 0;
		var _contador_globos_no_reventados = 0;

		var _text_opacity_def = 0.6;
		var _text_opacity = 0;
		var _text_opacity_dec = 0.06;
		var _text_font_size_def = 180;
		var _text_font_size = _text_font_size_def;
		var _text_font_size2 = 40;
		var _text_font_size_dec = 5;

		var _current_lvl = 1;

		ZonaA = function(){

		};
		ZonaA.prototype = {
			constructor:ZonaA,
			cargarElementos:function(){ 
				// load a resource
				for(var _idx in _elements){
					this.loadFile(_idx,_elements[_idx]);
				}
				var _this = this;
				//_this.setInterval(); 
				$(window).blur(
					function(evt){ 
						_this.blur();
					}
				);
				$(window).focus(
					function(evt){
						_this.focus();
					}
				); 
				$(w_ctouch).click(
					function(_evt){
						_evt.preventDefault();
						_this.checkCollisions();
					}
				);

				this.loadSound(); 

			},
			loadSound:function(){
				var audio_listener = new THREE.AudioListener();
				w_wgl.camera.add( audio_listener );

				_audioLoader.load( 'audio/ballo_pop2.ogg', function( buffer ) {
					
					for (var i = 0; i < 4; i++) {
						var _sound = new THREE.Audio( audio_listener ); 
						_sound.setBuffer( buffer );
						_sound.setLoop( false );
						_sound.setVolume( 0.5 ); 
						_sounds_ogg.push(_sound);
					}  
					$( "#master_volume" ).on( "slidechange",function(event, ui){
						for (var i = 0; i < 4; i++) {
							var _sound = _sounds_ogg[i];
							_sound.setVolume( ui.value/100 ); 
						}  
					});
				});
			},
			blur:function(){
				if(_interval_add_id){ 
					window.clearInterval(_interval_add_id);
					_interval_add_id = null;
				}
			},
			focus:function(){
				this.setInterval();
			},
			setInterval:function(){
				if(_lib_lvl && _lib_lvl!=1 && _lib_lvl!=2 && _lib_lvl!=3)return;
				if(_lib_game_paused)return;
				if(_lib_game_finish)return;
				if(!_interval_add_id){ 
					_interval_add_id = window.setInterval(_instance.addGlobo,_elapsed_time_globo);
				}
			},
			loadFile:function(idx,obj){
				_loader.load( 
					obj, 
					// Function when resource is loaded
					function ( geometry, materials ) { 
						var object;  
						for(var _index in materials){
							var _mat = materials[_index];
							if(idx=="globo"){ 
								_mat.morphTargets = true; 
							}
							_mat.side = THREE.FrontSide;
						} 
						
						if(idx=="globo"){ 
							_globo_materials = materials;
							object = new THREE.Mesh( geometry, materials[0] ); 
							console.log("globo started");
							object.position.y = 15; 
							window._globo_mast = _globo_master = object;
							object._lib_mixer = new THREE.AnimationMixer( object );
							var clip = THREE.AnimationClip.CreateFromMorphTargetSequence( 'globo', geometry.morphTargets, 30 );
							object._lib_mixer.clipAction( clip ).setDuration( 1 ).play();
						}else{
							object = new THREE.Mesh( geometry, materials ); 
							if(idx=="piso"){
								if(isMobile && isMobile.apple.device)object.receiveShadow = true;
							}else{ 
								//object.castShadow = true;
							}
							w_wgl.scene.add( object ); 
						}  
					},
					function(){

					},
					function(xhr){
						console.log(xhr);
					}
				);
			},
			cancelLoad:function(){

			},
			addGlobo:function(){
				if(_lib_lvl && _lib_lvl!=1 && _lib_lvl!=2 && _lib_lvl!=3)return;
				if(_lib_game_paused)return;
				if(_lib_game_finish)return;
				var _pass = w_wgl && w_wgl.scene && _globo_master;
				if(!_pass)return; 
				var _globo,_find=false;
				for(var _idx in _lib_globos){
					_globo = _lib_globos[_idx];
					if(!_globo.visible){
						_find = true;
						break;
					}
				}
				if(!_find){
					_globo = _globo_master.clone(); 
					if(isMobile && isMobile.apple.device)_globo.castShadow = true;
					//----------------------------------------------------------------
					_globo._lib_mixer = new THREE.AnimationMixer( _globo );
					var clip = THREE.AnimationClip.CreateFromMorphTargetSequence( 'globo', _globo.geometry.morphTargets, 30 );
					_globo._lib_mixer.clipAction( clip ).setDuration( 1 ).play();
					_globo._lib_boundingSphere1 = new THREE.Sphere(new THREE.Vector3(),3.1);
					_globo._lib_boundingSphere2 = new THREE.Sphere(new THREE.Vector3(0,1.88041,0),2.69594);
					_globo._lib_boundingSphere3 = new THREE.Sphere(new THREE.Vector3(0,-1.88041,0),2.69594);
					//----------------------------------------------------------------
					_lib_globos.push(_globo);
					w_wgl.scene.add(_globo);
				}
				var _randX = (Math.random()-0.5)*50;
				var _randZ = (Math.random()-0.5)*50;
				_randX = THREE.Math.clamp(_randX,-30,30);
				_randZ = THREE.Math.clamp(_randZ,-30,30);
				_globo.position.set(_randX,70,_randZ);
				_globo.visible =true; 
				_globo.material = _globo_materials[_mat_index];
				_mat_index++;
				if(_mat_index>_globo_materials.length-1){
					_mat_index = 0;
				}

				if(_current_lvl==2){
					if(_elapsed_time_globo>_elapsed_time_globo_min_medio){
						_elapsed_time_globo-=_elapsed_time_globo_dec_medio; 
					}
				}else if(_current_lvl==3){
					if(_elapsed_time_globo>_elapsed_time_globo_min_dificil){
						_elapsed_time_globo -= _elapsed_time_globo_dec_dificil; 
					}
				}else{
					if(_elapsed_time_globo>_elapsed_time_globo_min_facil){
						_elapsed_time_globo-=_elapsed_time_globo_dec_facil; 
					}
				} 
				_instance.blur();
				_instance.focus();
			},
			checkLvl:function(){
				if(_lib_lvl!=_current_lvl){
					_current_lvl = _lib_lvl;
					this.reset();
				}
			},
			reset:function(){
				_elapsed_time_globo = _elapsed_time_globo_def;
				for(var _idx in _lib_globos){
					var _globo = _lib_globos[_idx];
					_globo.visible = false;
				} 
				_contador_globos_reventados = 0;
				_contador_globos_no_reventados = 0;
				$("#trev_v").html("0");
			},
			render:function(){
				if(_lib_lvl && _lib_lvl!=1 && _lib_lvl!=2 && _lib_lvl!=3)return;
				this.checkLvl();
				if(_lib_game_paused)return;
				if(_lib_game_finish)return;
				if(window._lib_game_reset){
					this.reset();
					window._lib_game_reset = false;
				}
				for(var _idx in _lib_globos){
					var _globo = _lib_globos[_idx];
					if(_globo.visible){
						if(_globo && _globo._lib_mixer){
							_globo._lib_mixer.update( 0.01 );
						}
						if(_current_lvl==2){
							_globo.position.y-=0.38;
						}else if(_current_lvl==3){
							_globo.position.y-=0.46;
						}else{
							_globo.position.y-=0.3;
						}
						if(_globo.position.y<=5){
							_globo.visible = false;
							_contador_globos_no_reventados--;
						}
					} 
				} 
				if(_lib_touchedForFire){
					this.checkCollisions();
				}
				this.drawTexts();
				if(_current_lvl==2){
					if(_contador_globos_no_reventados<=-5){
						window._lib_game_finish = true;
						window._lib_force_exit_pointer_lock = true;
						$("#l_points").html(_contador_globos_reventados);
						$("#l_lvl").html("Intermedio");
						$("#modal_lost").modal("show");
					}
				}else if(_current_lvl==3){
					if(_contador_globos_no_reventados<=-10){
						window._lib_game_finish = true;
						window._lib_force_exit_pointer_lock = true;
						$("#l_points").html(_contador_globos_reventados);
						$("#l_lvl").html("Complicado");
						$("#modal_lost").modal("show");
					}
				}else{
					if(_contador_globos_no_reventados<0){
						window._lib_game_finish = true;
						window._lib_force_exit_pointer_lock = true;
						$("#l_points").html(_contador_globos_reventados);
						$("#l_lvl").html("Fácil");
						$("#modal_lost").modal("show");
					}
				} 

			},
			drawTexts:function(){
				this.drawTextGlobosReventados();
				this.drawTextGlobosNoReventados();
			},
			drawTextGlobosReventados:function(){ 
				if(_text_opacity>0){
					var _el = $("#txt1");
					_el.css("font-size",_text_font_size+"px");
					_el.html(_contador_globos_reventados);
					w_ctouch_ctx.fillStyle = 'rgba(0,0,0,'+_text_opacity+')';  
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.font = _text_font_size+"px 'Fredoka One', cursive";
					w_ctouch_ctx.fillText(_contador_globos_reventados,w_width/2-_el.outerWidth()/2,w_height/2+_el.outerHeight()/4);
					w_ctouch_ctx.closePath();  
					_text_opacity-=_text_opacity_dec;
					_text_font_size-=_text_font_size_dec;
				}
			},
			drawTextGlobosNoReventados:function(){  
					var _el = $("#txt1");
					_el.css("font-size",_text_font_size2+"px");
					_el.html(_contador_globos_no_reventados);
					w_ctouch_ctx.fillStyle = 'rgba(255,0,0,0.8)';  
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.font = _text_font_size2+"px 'Fredoka One', cursive";
					w_ctouch_ctx.fillText(_contador_globos_no_reventados,w_width-_el.outerWidth() - 25,w_height - _el.outerHeight() - 20);
					w_ctouch_ctx.closePath();   
			},
			checkCollisions:function(){ 
				if(_lib_game_paused)return;
				if(_lib_game_finish)return;
				this.setInterval(); 
				this.rayCastGlobos(); 
			},
			rayCastGlobos:function(){  
				if(!w_wgl || !w_wgl.camera)return; 
				var _line = {
					start:w_wgl.camera.position.clone(),
					_lib_vunit:w_wgl.camera.getWorldDirection().clone().normalize()
				}; 
				if(!_lib_globos)return;   
				for(var _idx in _lib_globos){
					var _globo = _lib_globos[_idx];
					if(_globo.visible){   

						var _bB1 = _globo._lib_boundingSphere1.clone();
						var _bB2 = _globo._lib_boundingSphere2.clone();
						var _bB3 = _globo._lib_boundingSphere3.clone();

						_bB1.center.add(_globo.position);
						_bB2.center.add(_globo.position);
						_bB3.center.add(_globo.position); 

						if(
							this.line3SphereIntersecction(_line,_bB1) || 
							this.line3SphereIntersecction(_line,_bB2) || 
							this.line3SphereIntersecction(_line,_bB3)
						){
							_globo.visible = false;
							_contador_globos_reventados ++;
							$("#trev_v").html(_contador_globos_reventados);
							if(_sounds_ogg.length>0){ 
								for (var i = 0; i < _sounds_ogg.length; i++) {
									var _sound = _sounds_ogg[i];
									if(!_sound.isPlaying){
										_sound.play();
										break;
									}
								} 
							}
							_text_opacity = _text_opacity_def;
							_text_font_size = _text_font_size_def;
							break;
						} 
					} 
				}
			},
			line3SphereIntersecction:function(line3,_sphere){
				var _r2 = 0;
				var _o = line3.start;
				var _c = 0;
				var _l = line3._lib_vunit;
				if(_sphere instanceof THREE.Sphere){
					_r2 = Math.pow(_sphere.radius,2);
					_c = _sphere.center.clone();
				}else{
					return false;
				}

				var _oc1 = _c.clone().sub(_o);
				_oc1 = Math.pow(_oc1.x,2)+Math.pow(_oc1.y,2)+Math.pow(_oc1.z,2);

				var _oc2 = _c.clone().sub(_o);
				_oc2 = _oc2.dot(_l);
				_oc2 = Math.pow(_oc2,2);

				var _result = _oc2 - _oc1 + _r2;
				if(_result>=0){return true;}
				return false;
			},
			test:function(){
				console.log("hello from ZonaA object");
			}
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new ZonaA();
					return _instance;
				}
				return null;
			}
		};
	}
)();


$(window).ready(
	function(evt){  
		setCanvasDim();
		$(window).resize(
			function(evt){
				setCanvasDim();
			}
		);
		(window.appMain = appgame.Main).start();
	}
); 
function setCanvasDim(){
	window.w_width = window.innerWidth;
	window.w_height = window.innerHeight;
	$("canvas").each(
		function(index,obj){
			var _this = $(obj);
			var _id = _this.attr("id");
			if(
				_id!="webgl" &&  
				_id!="canvas-assets" &&  
				_id!="canvas-touch" 
			){
				return;
			}
			_this.css("width",w_width+"px").css("height",w_height+"px");
			_this.attr("width",w_width).attr("height",w_height);
		}
	);
}

var appgame = appgame || {};
appgame.Main = (
	function(){

		var _instance = null;

		var _router = null;
		var _zona_a = null;
		var _ambientLight = new THREE.AmbientLight( 0xeeeeee); 
		var _directionalLight = new THREE.DirectionalLight( 0xaaaaaa, 0.1 );
		var _directionalLight2 = new THREE.DirectionalLight( 0xaaaaaa, 0.3 );
		var _directionalLight3 = new THREE.DirectionalLight( 0xaaaaaa, 0.3 );

		
		var _setCastShadowDirLight = function(_dirLight){
			var _d = 100;
			_dirLight.castShadow = true;
			_dirLight.shadow.camera.left = - _d;
			_dirLight.shadow.camera.right = _d;
			_dirLight.shadow.camera.top = _d;
			_dirLight.shadow.camera.bottom = - _d;
			_dirLight.shadow.camera.near = 1;
			_dirLight.shadow.camera.far = 100;
			_dirLight.shadow.mapSize.x = 1024;
			_dirLight.shadow.mapSize.y = 1024;
		};
		if(isMobile && isMobile.apple.device){
			_setCastShadowDirLight(_directionalLight);
			_setCastShadowDirLight(_directionalLight2);
			_setCastShadowDirLight(_directionalLight3);
		}

		var _cameraZPosInit = 30;
		var _cameraYPosInit = 3;
		var _cameraYPosInit_min = _cameraYPosInit;
		var _cameraXPosInit = 0;

		var _cameraAngle_def = 60;
		var _cameraAngle = _cameraAngle_def;

		var _fullScreen = null;
		var _playPause = null;
		var _cameraControl = null; 
		var _stats = null;

		_debug_touch = true;

		window._lib_game_finish = false;

		Main = function(){ 

			window.w_wgl 		= document.getElementById("webgl");
 			window.w_cassets 	= document.getElementById("canvas-assets");
 			window.w_ctouch 	= document.getElementById("canvas-touch");
 			if(window.w_cassets && window.w_cassets.getContext)window.w_cassets_ctx = window.w_cassets.getContext("2d");
 			if(window.w_ctouch && window.w_ctouch.getContext)window.w_ctouch_ctx = window.w_ctouch.getContext("2d");

		};
		Main.prototype = {
			constructor:Main,
			start:function(){
				window._lib_lvl = 1;
				var scene = new THREE.Scene();
				scene.background = new THREE.Color(0.5,0.5,0.9);
				var camera = new THREE.PerspectiveCamera( _cameraAngle, w_width / w_height, 0.1, 1000 );

				var renderer = new THREE.WebGLRenderer({canvas:w_wgl});
				if(isMobile && isMobile.apple.device){
					renderer.shadowMap.enabled = true;
				} 
				renderer.setSize(w_width,w_height);
				renderer.setPixelRatio(window.devicePixelRatio);
  
  				_directionalLight2.position.set(-5,10,5); 
  				_directionalLight2.target = new THREE.Object3D();
  				_directionalLight2.target.position.set(0,0,0);
  
  				_directionalLight3.position.set(5,10,-5); 
  				_directionalLight3.target = new THREE.Object3D();
  				_directionalLight3.target.position.set(0,0,0);

				scene.add( _ambientLight );
				scene.add( _directionalLight );
				scene.add( _directionalLight2 );
				scene.add( _directionalLight2.target );
				scene.add( _directionalLight3 );
				scene.add( _directionalLight3.target );
				camera.position.set(_cameraXPosInit,_cameraYPosInit,_cameraZPosInit);
				camera.lookAt(new THREE.Vector3());

				w_wgl.scene = scene;
				w_wgl.camera = camera;
				w_wgl.renderer = renderer; 
				w_wgl.playing = false;
				w_wgl.rqsAnim = true;

				w_wgl._lib_currTime = Date.now();
				w_wgl._lib_lastTime = w_wgl._lib_currTime;
 

				var _this = this;
				$(window).resize(
					function(evt){
						_this.resize();
					}
				);
				$(window).focus(
					function(evt){
						_this.focus();
					}
				);
				$(window).blur(
					function(evt){ 
						_this.blur();
					}
				);

				w_ctouch.addEventListener("touchstart",_this.touchstart,false);
				w_ctouch.addEventListener("touchend",_this.touchend,false);
				w_ctouch.addEventListener("touchcancel",_this.touchcancel,false);
				w_ctouch.addEventListener("touchleave",_this.touchleave,false);
				w_ctouch.addEventListener("touchmove",_this.touchmove,false);

				_stats = new Stats();
				_stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
				document.body.appendChild( _stats.dom );

				_this.animate();

				_zona_a = appgame.ZonaA.instance(); 
				(_fullScreen = appgame.FullScreen.instance()).start(); 
				(_playPause = appgame.PlayPause.instance()).start(); 
				(_cameraControl = appgame.CameraControl.instance()).start(); 

				_this.mostrarPaneldeBienvenida();
				_this.procesarNivelJuego();

				_this.ctrlButton();
				_this.parseVendor();
				$("#play").click(function(_evt){
					$("#modal_bienvenida").modal("hide");
						window._lib_game_paused = false;
						window._lib_game_finish = false;
						window._lib_game_reset = true;
				});

			},
			ctrlButton:function(){
				$("body").append("<div id='settings'></div>"); 
				$("#settings").click(
					function(_evt){ 
						$("#modal_bienvenida").modal("show");
						window._lib_game_paused = true;
					}
				);
			},
			touchstart:function(_evt){ 
				if(_playPause)_playPause.touch("s",_evt);
				if(_cameraControl)_cameraControl.touch("s",_evt);
			},
			touchend:function(_evt){ 
				if(_playPause)_playPause.touch("e",_evt);
				if(_cameraControl)_cameraControl.touch("e",_evt);
			},
			touchcancel:function(_evt){ 
				if(_playPause)_playPause.touch("c",_evt);
				if(_cameraControl)_cameraControl.touch("c",_evt);
			},
			touchleave:function(_evt){ 
				if(_playPause)_playPause.touch("l",_evt);
				if(_cameraControl)_cameraControl.touch("l",_evt);
			},
			touchmove:function(_evt){ 
				if(_playPause)_playPause.touch("m",_evt);
				if(_cameraControl)_cameraControl.touch("m",_evt);
			}, 
			animate:function(){
				if(w_wgl.rqsAnim){
					w_wgl.rqs_anim_id = requestAnimationFrame( _instance.animate );
				}  
				_stats.begin();
				if(!w_wgl._lib_currTime){
					w_wgl._lib_currTime = Date.now();
					w_wgl._lib_lastTime = w_wgl._lib_currTime; 
				}
				w_wgl._lib_lastTime = w_wgl._lib_currTime;
				w_wgl._lib_currTime = Date.now();

				if(w_ctouch_ctx)w_ctouch_ctx.clearRect(0,0,w_width,w_height);
				if(_fullScreen)_fullScreen.render();
				if(_playPause)_playPause.render();
				if(_cameraControl)_cameraControl.render(); 
				if(_zona_a)_zona_a.render();
				if(w_wgl.camera.position.y<_cameraYPosInit_min){
					w_wgl.camera.position.y = _cameraYPosInit_min;
				}
				w_wgl.renderer.render(w_wgl.scene, w_wgl.camera);
				_stats.end();
			},
			blur:function(){
				//this.stopAnimation();
			},
			focus:function(){
				this.playAnimation();
			}, 
			stopAnimation:function(){
				w_wgl.rqsAnim = false;
				if(w_wgl.rqs_anim_id){
					cancelAnimationFrame(w_wgl.rqs_anim_id);
				}
			},
			playAnimation:function(){
				if(w_wgl.rqs_anim_id)cancelAnimationFrame(w_wgl.rqs_anim_id);
				w_wgl.rqsAnim = true;
				this.animate();
			},
			resize:function(){
				w_wgl.camera.aspect = w_width / w_height;
    			w_wgl.camera.updateProjectionMatrix();
				w_wgl.renderer.setSize(w_width,w_height);
				if(_fullScreen)_fullScreen.resize();
				if(_playPause)_playPause.resize();
				if(_cameraControl)_cameraControl.resize();
			},
			parseVendor:function(){
				if(!window._lib_vendor && w_wgl.renderer){
					var ctx = w_wgl.renderer.getContext();
					var info = w_wgl.renderer.extensions.get("WEBGL_debug_renderer_info");
					window._lib_vendor = {};
					window._lib_vendor.vendor 		= ctx.getParameter(ctx.VENDOR);
					window._lib_vendor.renderer 	= ctx.getParameter(ctx.RENDERER);
					window._lib_vendor.u_vendor 	= ctx.getParameter(info.UNMASKED_VENDOR_WEBGL);
					window._lib_vendor.u_renderer 	= ctx.getParameter(info.UNMASKED_RENDERER_WEBGL);

					$("#cpu_info1").html(_lib_vendor.vendor);
					$("#cpu_info2").html(_lib_vendor.renderer);
					$("#cpu_info3").html(_lib_vendor.u_vendor);
					$("#cpu_info4").html(_lib_vendor.u_renderer); 
					$("#cpu_info5").html("#Procesadores: "+navigator.hardwareConcurrency); 

				}
			},
			mostrarPaneldeBienvenida:function(){
				$("#modal_bienvenida").modal("show");
				window._lib_game_paused = true;
				if(window.location && !/localhost/.test(window.location.hostname)){
					var _str = ''+
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>'+
						'<ins class="adsbygoogle"'+
						     'style="display:block;"'+
						     'data-ad-format="fluid"'+
						     'data-ad-layout-key="-8m+w-aj+dk+bi"'+
						     'data-ad-client="ca-pub-6209186190244737"'+
						     'data-ad-slot="7742196875"></ins>'+
						'<script>'+
						     '(adsbygoogle = window.adsbygoogle || []).push({});'+
						'</script>';

					var _str2 = ''+
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>'+
						'<ins class="adsbygoogle"'+
					     'style="display:block;"'+
					     'data-ad-format="fluid"'+
					     'data-ad-layout-key="-8n+x-ah+dn+as"'+
					     'data-ad-client="ca-pub-6209186190244737"'+
					     'data-ad-slot="8801742078"></ins>'+
						'<script>'+
						     '(adsbygoogle = window.adsbygoogle || []).push({});'+
						'</script>'; 

					$("#ct-1").html(_str);
					$("#ct-2").html(_str2); 
				}
			},
			procesarNivelJuego:function(){ 
				this.cargarZonaA();
			},
			cancelarCargaActual:function(){
				this.eliminarObjetosDeEscena();
			},
			eliminarObjetosDeEscena:function(){
				if(w_wgl.scene){ 
				} 
			},
			cargarZonaA:function(){
				if(_zona_a)_zona_a.cargarElementos();
			}
		};

		return {
			start:function(){
				if(!_instance){
					_instance = new Main();
					_instance.start();
				}
			}
		};
	}
)();


